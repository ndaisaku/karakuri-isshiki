//
//  MainPage.h
//  Test_box2d
//
//  Created by mac on 11/07/24.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "CCTransitionHelper.h"
#import "SimpleAudioEngine.h"
#import "finish.h"
#import "TileScene.h"
#import "settei.h"
#import "SoundPlayer.h"

// MainPage
@interface MainPage : CCLayer <UIAlertViewDelegate> 
{
	b2World* world;
	GLESDebugDraw *m_debugDraw;
    CCTexture2D *texture[18];
    b2Fixture *_bottomFixture;
    
    CCSprite *Movie_Sprite[10];
    
    CCSprite *P19_Movie_Sprite[6];
    
    CCSprite *textBG_Sprite;
    CCMenuItemImage *maryItem3;
    CCMenuItemImage *maryItem4;
    b2Body* m_bodies[7];
	b2Joint* m_joints[12];
    
    b2Fixture *_paddleFixture[7];
    
    b2MouseJoint *_mouseJoint;
    b2Body *body;
    b2Body* groundBody;
    
    b2RevoluteJoint* m_joint1;
	b2PrismaticJoint* m_joint2;
    
    CCSprite *spriteBack;
    
    CGPoint startPt;
    BOOL touche;
    BOOL kane;
    CGPoint kaneTouch;
    int kanecount;
    
    ALuint narr;
    ALuint se;
        
    CGFloat fadeTimeTaken;
	CGFloat fadeDuration;
	CGFloat fadeStartVolume;
	CGFloat fadeEndVolume;
    NSString *narrapath;
    bool kiraikae;
    
    SimpleAudioEngine *narration;
    
    bool firest1;
    bool firest2;
    bool firest3;
    bool firest4;
    bool firest5;
    bool firest6;
    
    //シーン14:焚き火の音判定;
    bool SoundContorl_takibi;
    
    //シーン16:魔王のパワー;
    int maouPower;
    
    //シーン16:魔王のリピートアクション;
    id MamonoRep16[4];
    
    //パーティクル
    CCParticleSystem *particles[8];
    
}

// returns a CCScene that contains the MainPage as the only child
+(id) scene:(NSInteger)Page:(NSInteger)sette1:(NSInteger)sette2:(NSInteger)sette3:(BOOL)nextPage;
// adds a new sprite at a given coordinate
-(void)PageEffect;
-(void)scene14:(NSInteger)num;
-(void)scene16:(NSInteger)num:(CGPoint)p;
-(void)scene16Funsai;
-(void)scene16TatumakiStop;
-(void)addNewSpriteWithJoint2;
-(void)addNewSpriteWithJoint:(CGPoint)p;
-(void)preloadParticleEffect:(NSString *)particleFile;
@end
