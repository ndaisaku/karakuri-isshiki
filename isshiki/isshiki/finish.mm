//
//  finish.m
//  Test_box2d
//
//  Created by soei-ima on 11/08/08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "finish.h"
NSInteger page2;
@implementation finish
+(id) scene:(NSInteger)Page{
    page2 = Page;
    CCScene *scene = [CCScene node];
    finish *layer = [finish node];
    [scene addChild: layer];
    return scene;
}
-(id) init{
	if( (self=[super init])) {
        
        
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"lastButton.plist" textureFilename:@"lastButton.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"lastButton.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite *buttonSprite1 = [[CCSprite spriteWithSpriteFrameName:@"top_btn.png"] retain];
        CCSprite *buttonSprite2 = [[CCSprite spriteWithSpriteFrameName:@"top_btn_On.png"] retain];
        CCSprite *buttonSprite3 = [[CCSprite spriteWithSpriteFrameName:@"top_btn_On.png"] retain];
        
        CCSprite *buttonSprite4 = [[CCSprite spriteWithSpriteFrameName:@"credit_btn.png"] retain];
        CCSprite *buttonSprite5 = [[CCSprite spriteWithSpriteFrameName:@"credit_btn_On.png"] retain];
        CCSprite *buttonSprite6 = [[CCSprite spriteWithSpriteFrameName:@"credit_btn_On.png"] retain];
        
        CCSprite *buttonSprite7 = [[CCSprite spriteWithSpriteFrameName:@"stop01ButtonOff.png"] retain];
        CCSprite *buttonSprite8 = [[CCSprite spriteWithSpriteFrameName:@"stop01ButtonOn.png"] retain];
        CCSprite *buttonSprite9 = [[CCSprite spriteWithSpriteFrameName:@"stop01ButtonOn.png"] retain];
        
        CCSprite *buttonSprite10 = [[CCSprite spriteWithSpriteFrameName:@"stop02ButtonOff.png"] retain];
        CCSprite *buttonSprite11 = [[CCSprite spriteWithSpriteFrameName:@"stop02ButtonOn.png"] retain];
        CCSprite *buttonSprite12 = [[CCSprite spriteWithSpriteFrameName:@"stop02ButtonOn.png"] retain];
        
        CCSprite *buttonSprite13 = [[CCSprite spriteWithSpriteFrameName:@"urlButtonOff.png"] retain];
        CCSprite *buttonSprite14 = [[CCSprite spriteWithSpriteFrameName:@"urlButtonOn.png"] retain];
        CCSprite *buttonSprite15 = [[CCSprite spriteWithSpriteFrameName:@"urlButtonOn.png"] retain];
        
        CCSprite *buttonSprite16 = [[CCSprite spriteWithSpriteFrameName:@"mapButtonOff.png"] retain];
        CCSprite *buttonSprite17 = [[CCSprite spriteWithSpriteFrameName:@"mapButtonOn.png"] retain];
        CCSprite *buttonSprite18 = [[CCSprite spriteWithSpriteFrameName:@"mapButtonOn.png"] retain];
        
        
        if(page2 == 1){
            texture = [[CCTextureCache sharedTextureCache] addImage:@"owari.pvr.gz"];
            CCSprite *sprite = [CCSprite spriteWithTexture:texture];
            sprite.anchorPoint = CGPointMake(0, 0);
            [self addChild:sprite];
            
            CCMenuItemImage *btn = [CCMenuItemImage 
                                    itemWithNormalImage:@"title_next.pvr.gz" 
                                    selectedImage: @"title_next.pvr.gz"
                                    target:self
                                    selector:@selector(pressPage:)];
            btn.tag = 1;
            btn.position = ccp(0, -330);
            
            CCMenu *menu = [CCMenu menuWithItems:btn, nil];
            [self addChild:menu];
            
            
            
        }else if(page2 == 2){
            
            texture = [[CCTextureCache sharedTextureCache] addImage:@"credit.pvr.gz"];
            CCSprite *sprite = [CCSprite spriteWithTexture:texture];
            sprite.anchorPoint = CGPointMake(0, 0);
            [self addChild:sprite];
            
            CCMenuItemImage *btn = [CCMenuItemImage 
                                    itemWithNormalImage:@"title_next.pvr.gz" 
                                    selectedImage: @"title_next.pvr.gz"
                                    target:self
                                    selector:@selector(pressPage:)];
            btn.tag = 1;
            btn.position = ccp(0, -330);
            
            CCMenu *menu = [CCMenu menuWithItems:btn, nil];
            [self addChild:menu];
            
            
        }else if(page2 == 3 || page2 == 4){
            
            if(page2 == 3){
                texture = [[CCTextureCache sharedTextureCache] addImage:@"spot01.pvr.gz"];
                CCSprite *sprite = [CCSprite spriteWithTexture:texture];
                sprite.anchorPoint = CGPointMake(0, 0);
                [self addChild:sprite];
            }else{
                texture = [[CCTextureCache sharedTextureCache] addImage:@"spot02.pvr.gz"];
                CCSprite *sprite = [CCSprite spriteWithTexture:texture];
                sprite.anchorPoint = CGPointMake(0, 0);
                [self addChild:sprite];
            }
            
            CCMenuItemSprite *btn = [CCMenuItemSprite 
                                     itemWithNormalSprite:buttonSprite1
                                     selectedSprite:buttonSprite2
                                     disabledSprite:buttonSprite3
                                     target:self
                                     selector:@selector(pressPage:)];
            btn.tag = 2;
            btn.position = ccp(110, -330);
            
            CCMenuItemSprite *btn2 = [CCMenuItemSprite 
                                      itemWithNormalSprite:buttonSprite4
                                      selectedSprite:buttonSprite5
                                      disabledSprite:buttonSprite6
                                      target:self
                                      selector:@selector(pressPage:)];
            btn2.tag = 3;
            btn2.position = ccp(-110, -330);
            
            CCMenuItemSprite *btn3 = [CCMenuItemSprite 
                                      itemWithNormalSprite:buttonSprite13
                                      selectedSprite:buttonSprite14
                                      disabledSprite:buttonSprite15
                                      target:self
                                      selector:@selector(pressPage:)];
            btn3.tag = 4;
            btn3.position = ccp(275, -170);
            
            
            CCMenuItemSprite *spotPlace1 = [CCMenuItemSprite 
                                            itemWithNormalSprite:buttonSprite7
                                            selectedSprite:buttonSprite8
                                            disabledSprite:buttonSprite9
                                            target:self
                                            selector:@selector(pressPage:)];
            spotPlace1.position = ccp(-320, -170);
            spotPlace1.tag =5;
            
            
            CCMenuItemSprite *spotPlace2 = [CCMenuItemSprite 
                                            itemWithNormalSprite:buttonSprite10
                                            selectedSprite:buttonSprite11
                                            disabledSprite:buttonSprite12
                                            target:self
                                            selector:@selector(pressPage:)];
            spotPlace2.position = ccp(-45, -170);
            spotPlace2.tag =6;
            
            CCMenuItemSprite *mapButton = [CCMenuItemSprite 
                                           itemWithNormalSprite:buttonSprite16
                                           selectedSprite:buttonSprite17
                                           disabledSprite:buttonSprite18
                                           target:self
                                           selector:@selector(pressPage:)];
            mapButton.position = ccp(400, -90);
            mapButton.tag =7;
            
            CCMenu *menu = [CCMenu menuWithItems:btn,btn2,btn3,spotPlace1,spotPlace2,mapButton,nil];
            [self addChild:menu];
            
        }
        
    }
    return self;
}

//設定用のファイルパス-------------------------------------------------------------------
- (NSString *)dataFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return [documentsDirectory stringByAppendingPathComponent:@"settei.plist"];
}

-(void)pressPage:(id)sender{
    if ([sender tag] == 1) {
        NSInteger Page_count = page2;
        Page_count += 1;
        CCScene *scene = [finish scene:Page_count];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
    }else if ([sender tag] == 2) {
        CCScene *scene = [TileScene scene:3];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
        
    }else if([sender tag] == 3){
        NSInteger Page_count = page2;
        Page_count -= 1;
        CCScene *scene = [finish scene:Page_count];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageBackward duration:0.5];
    }else if([sender tag] == 4){
        
        NSString *url = [[NSString alloc] initWithString:@"http://www.city.nishio.aichi.jp/"];
        [self webBrowser:url];
        
    }else if([sender tag] == 5){
        if(page2 == 4){
            NSInteger Page_count = page2;
            Page_count -= 1;
            CCScene *scene = [finish scene:Page_count];
            [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageBackward duration:0.5];
        }
        
    }else if([sender tag] == 6){
        if(page2 == 3){
            NSInteger Page_count = page2;
            Page_count += 1;
            CCScene *scene = [finish scene:Page_count];
            [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
        }
    }else if([sender tag] == 7){
        if(page2 == 3){
            NSString *url = [[NSString alloc] initWithString:@"http://g.co/maps/zc7b8"];
            [self webBrowser:url];
        }else{
            NSString *url = [[NSString alloc] initWithString:@"http://g.co/maps/p5sye"];
            [self webBrowser:url];
        }
    }
}
-(void)webBrowser:(NSString *)urltext{
    NSString *path = [[NSString alloc] initWithFormat:@"%@",urltext];
    NSURL *url = [NSURL URLWithString:path];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    
    webView_ = [[UIWebView alloc] initWithFrame:CGRectMake(0, 46, 1024, 722)];
    
    webView_.bounds = CGRectMake(0, 46, 1024, 722);
    webView_.scalesPageToFit = YES;
    
    //UIToolbarの生成
    toolBar = [[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 1024, 46)] autorelease];
    
    
    //インジケーターの追加
    /* activityInducator_ = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
     activityInducator_.frame = CGRectMake(0, 0, 20, 20);
     UIBarButtonItem *inducator = [[UIBarButtonItem alloc] initWithCustomView:activityInducator_];
     UIBarButtonItem *adjustment =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
     */
    
    //戻るボタン
    UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
    goBackButton.title = @"閉じる";
    goBackButton.style = UIBarButtonItemStyleBordered;
    goBackButton.target = self;
    goBackButton.action = @selector(goBack);
    
    NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton, nil];
    [goBackButton release];
    [toolBar setItems:elements animated:YES];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown 
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    //webView_.delegate =self;
    [[[CCDirector sharedDirector] view] addSubview:webView_];
    [[[CCDirector sharedDirector] view] addSubview:toolBar];
    
    [UIView commitAnimations];
    [webView_ loadRequest:req];
}
/*-(void)webViewDidStartLoad:webView_{
 [activityInducator_ startAnimating];
 }
 
 -(void)webViewDidFinishLoad:webView_{
 [activityInducator_ stopAnimating];
 }
 - (void)webView:webView_ didFailLoadWithError:(NSError*)error {
 if(([[error domain]isEqual:NSURLErrorDomain])
 && ([error code]!=NSURLErrorCancelled)) {
 UIAlertView* alert = [[UIAlertView alloc]
 initWithTitle:nil
 message:@"エラー：インターネットに繋がっておりません"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 [alert release];
 }
 }
 */

- (void)goBack {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    [webView_ removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView commitAnimations];
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
}
// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [webView_ release];
	[super dealloc];
    
}

@end
