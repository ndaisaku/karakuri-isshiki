//
//  TileScene.h
//  Test_box2d
//
//  Created by soei-ima on 11/07/31.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "CCTransitionHelper.h"
#import "settei.h"
#import "MainPage.h"
#import "SimpleAudioEngine.h"

@interface TileScene : CCLayer {
    bool start;
    CCTexture2D *texture;
    NSInteger optionnum[3];
    ALint titleVoice;
}

+(id) scene:(NSInteger)Page;
-(void)pagekirikae;
- (NSString *)dataFilePath;
-(void)setteiRead;
@end
