//
//  SceneManager.m
//  Test_box2d
//
//  Created by soei-ima on 11/08/04.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SceneManager.h"


@interface SceneManager()
+(void) go: (CCLayer *) layer:(NSInteger)Page:(NSInteger)Choice;
+(void) goPlay:(int) Page:(int) Choice;
+(CCScene *) wrap: (CCLayer *) layer:(NSInteger)Page;

@end

@implementation SceneManager
+(void) goPlay:(int) Page:(int) Choice{
    CCLOG(@"Page%d",Page);
    Page +=1;
	CCLayer *layer = [MainPage node];
	[SceneManager go: layer: Page: Choice];
}

+(void) go: (CCLayer *) layer:(NSInteger)Page:(NSInteger) Choice{
	CCDirector *director = [CCDirector sharedDirector];
	CCScene *newScene = [SceneManager wrap:layer:Page];
    CCLOG(@"Choice%d",Choice);
    CCLOG(@"Page%d",Page);
    if(Choice == 1){
        if ([director runningScene]) {
            [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.3];
        }else {
            [CCTransitionHelper runWithScene:newScene transitionType:TransitionTypePageForward duration:0.3];
        }
    }else{
        if ([director runningScene]) {
            [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.3];
        }else {
            [CCTransitionHelper runWithScene:newScene transitionType:TransitionTypePageBackward duration:0.3];
        } 
    }
}

+(CCScene *) wrap: (CCLayer *) layer:(int) Page{
	CCScene *newScene = [CCScene node];
	[newScene addChild: layer];
	return newScene;
}

@end