//
//  SceneManager.h
//  Test_box2d
//
//  Created by soei-ima on 11/08/04.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MainPage.h"
#import "CCTransitionHelper.h"

@interface SceneManager : NSObject {
}
    
+(void) go: (CCLayer *) layer:(NSInteger) Page:(NSInteger)Choice;
+(void) goPlay:(int) Page:(int) Choice;

@end
