//
//  settei.m
//  Test_box2d
//
//  Created by soei-ima on 11/08/08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "settei.h"
@implementation settei
+(id) scene{
    CCScene *scene = [CCScene node];
    settei *layer = [settei node];
    [scene addChild: layer];
    return scene;
}


-(id) init{
	if( (self=[super init])) {
        
        [self setteiRead];
        
        CCSprite *sprite = [CCSprite spriteWithFile:@"settei.pvr.gz"];
        sprite.anchorPoint = CGPointMake(0, 0);
        [self addChild:sprite];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"otherButton.plist" textureFilename:@"otherButton.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"otherButton.pvr.gz"];
        [self addChild:spriteSheet];

        CCSprite *buttonSprite1 = [[CCSprite spriteWithSpriteFrameName:@"hpButton.png"] retain];
        
        CCSprite *buttonSprite2 = [[CCSprite spriteWithSpriteFrameName:@"hpButtonOn.png"] retain];
        
        CCSprite *buttonSprite3 = [[CCSprite spriteWithSpriteFrameName:@"hpButtonOn.png"] retain];
        
        CCSprite *buttonSprite4 = [[CCSprite spriteWithSpriteFrameName:@"yukariButton.png"] retain];
        
        CCSprite *buttonSprite5 = [[CCSprite spriteWithSpriteFrameName:@"yukariButtonOn.png"] retain];
        
        CCSprite *buttonSprite6 = [[CCSprite spriteWithSpriteFrameName:@"yukariButtonOn.png"] retain];
        
        CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                      itemWithNormalImage:@"on.png" 
                                      selectedImage: @"on.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem1.position = ccp(12, 160);
        maryItem1.tag =1;

        
        CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                      itemWithNormalImage:@"off.png" 
                                      selectedImage: @"off.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem2.position = ccp(173, 160);
        maryItem2.tag =2;
        
        CCMenuItemImage *maryItem3 = [CCMenuItemImage 
                                      itemWithNormalImage:@"on.png" 
                                      selectedImage: @"on.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem3.position = ccp(12, 66);
        maryItem3.tag =3;
        
        
        CCMenuItemImage *maryItem4 = [CCMenuItemImage 
                                      itemWithNormalImage:@"off.png" 
                                      selectedImage: @"off.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem4.position = ccp(173, 66);
        maryItem4.tag =4;
        
        
        CCMenuItemImage *maryItem5 = [CCMenuItemImage 
                                      itemWithNormalImage:@"on.png" 
                                      selectedImage: @"on.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem5.position = ccp(12, -29);
        maryItem5.tag =5;
        
        
        CCMenuItemImage *maryItem6 = [CCMenuItemImage 
                                      itemWithNormalImage:@"off.png" 
                                      selectedImage: @"off.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem6.position = ccp(173, -29);
        maryItem6.tag =6;
        
        CCMenuItemImage *maryItem7 = [CCMenuItemImage 
                                      itemWithNormalImage:@"back_btn.pvr.gz" 
                                      selectedImage: @"back_btn.pvr.gz"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem7.position = ccp(0, -310);
        maryItem7.tag =7;
        
        
        CCMenu *menu2 = [CCMenu menuWithItems:maryItem1,maryItem2,maryItem3,maryItem4,maryItem5,maryItem6,maryItem7,nil];
        [self addChild:menu2 z:2];
        
        sprite2[0] = [CCSprite spriteWithFile:@"setteiwaku.pvr.gz"];
        if(optionnum3[0] == 0){
            sprite2[0].position = ccp(524, 543);
        }else{
            sprite2[0].position = ccp(683, 543);
        }
        [self addChild:sprite2[0]];
        
        sprite2[1] = [CCSprite spriteWithFile:@"setteiwaku.pvr.gz"];
        if(optionnum3[1] == 0){
            sprite2[1].position = ccp(524, 450);
        }else{
            sprite2[1].position = ccp(683, 450);
        }
        [self addChild:sprite2[1]];
        
        sprite2[2] = [CCSprite spriteWithFile:@"setteiwaku.pvr.gz"];
        if(optionnum3[2] == 0){
            sprite2[2].position = ccp(524, 355);
        }else{
            sprite2[2].position = ccp(683, 355);
        }
        [self addChild:sprite2[2]];
        
        
        //HPとゆかりの地ボタン
        CCMenuItemSprite *maryItem8 = [CCMenuItemSprite 
                                       itemWithNormalSprite:buttonSprite1
                                       selectedSprite:buttonSprite2
                                       disabledSprite:buttonSprite3
                                       target:self
                                       selector:@selector(pressMenuItem2:)];
        maryItem8.position = ccp(0, 0);
        maryItem8.tag =1;
        
        
        CCMenuItemSprite *maryItem9 = [CCMenuItemSprite 
                                       itemWithNormalSprite:buttonSprite4
                                       selectedSprite:buttonSprite5
                                       disabledSprite:buttonSprite6
                                       target:self
                                      selector:@selector(pressMenuItem2:)];
        maryItem9.position = ccp(0, -70);
        maryItem9.tag =2;
        
        CCMenu *menu3 = [CCMenu menuWithItems:maryItem8,maryItem9,nil];
        menu3.position = ccp(522, 260);
        [self addChild:menu3 z:3];
        
    }
    return self;
}

-(void)pressMenuItem:(id)sender{
    if([sender tag] == 1){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(524, 543)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[0] runAction:ease];
        optionnum3[0]=0;
        
    }else if([sender tag] == 2){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(683, 543)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[0] runAction:ease];
        optionnum3[0]=1;
        
    }else if([sender tag] == 3){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(524, 450)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[1] runAction:ease];
         optionnum3[1]=0;
    }else if([sender tag] == 4){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(683, 450)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[1] runAction:ease];
         optionnum3[1]=1;
    }else if([sender tag] == 5){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(524, 355)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[2] runAction:ease];
         optionnum3[2]=0;
    }else if([sender tag] == 6){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(683, 355)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[2] runAction:ease];
         optionnum3[2]=1;
    }else if([sender tag] == 7){
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for (int i=0; i<3; i++) {
            [array addObject:[NSNumber numberWithInt:optionnum3[i]]];
            [array writeToFile:[self dataFilePath] atomically:YES];
        }
        [array release];
        
        CCScene *scene = [TileScene scene:3];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
    }
    
    
}

-(void)pressMenuItem2:(id)sender{
    if([sender tag] == 1){
        NSString *path = [[NSString alloc] initWithFormat:@"http://www.karakuri-books.com/"];
        NSURL *url = [NSURL URLWithString:path];
        NSURLRequest *req = [NSURLRequest requestWithURL:url];
        
        
        webView_ = [[UIWebView alloc] initWithFrame:CGRectMake(0, 46, 1024, 768)];
        
        webView_.bounds = CGRectMake(0, 46, 1024, 768);
        webView_.scalesPageToFit = YES;
        
        //UIToolbarの生成
        toolBar = [[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 1024, 46)] autorelease];
        
        
        //インジケーターの追加
         activityInducator_ = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
         activityInducator_.frame = CGRectMake(0, 0, 20, 20);
         UIBarButtonItem *inducator = [[UIBarButtonItem alloc] initWithCustomView:activityInducator_];
         UIBarButtonItem *adjustment =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
         
        
        //戻るボタン
        UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
        goBackButton.title = @"閉じる";
        goBackButton.style = UIBarButtonItemStyleBordered;
        goBackButton.target = self;
        goBackButton.action = @selector(goBack);
        
        NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton,adjustment,inducator,nil];
        [goBackButton release];
        [toolBar setItems:elements animated:YES];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.0f];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown 
                               forView:[[CCDirector sharedDirector] view] cache:YES];
        webView_.delegate =self;
        [[[CCDirector sharedDirector] view] addSubview:webView_];
        [[[CCDirector sharedDirector] view] addSubview:toolBar];
        
        [UIView commitAnimations];
        [webView_ loadRequest:req];
       
        
    }else if([sender tag] == 2){
        CCScene *scene = [finish scene:3];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];

    }
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

}

//設定ファイル読み込み
-(void)setteiRead{
	NSString *filePath = [self dataFilePath];
	if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
		NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (int i = 0; i<3; i++) {
            NSString *Settei  = [[NSString alloc] init];
            Settei = [array objectAtIndex:i];
            optionnum3[i] = [Settei intValue];
        }
        [array release];
	}
	
}

-(void)webViewDidStartLoad:webView_{
 [activityInducator_ startAnimating];
}
 
 -(void)webViewDidFinishLoad:webView_{
 [activityInducator_ stopAnimating];
}

//webViewを閉じる-------------------------------------------------------------------
- (void)goBack {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    [webView_ removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView commitAnimations];
}

//設定用のファイルパス-------------------------------------------------------------------
- (NSString *)dataFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return [documentsDirectory stringByAppendingPathComponent:@"settei.plist"];
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCTextureCache sharedTextureCache] removeAllTextures];
    [webView_ release];
    [activityInducator_ release];
    //[toolBar release];
	[super dealloc];
}

@end
