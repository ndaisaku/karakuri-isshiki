//
//  finish.h
//  Test_box2d
//
//  Created by soei-ima on 11/08/08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CCTransitionHelper.h"
#import "cocos2d.h"
#import "TileScene.h"
#import <UIKit/UIWebView.h>

@interface finish : CCLayer{
    bool start;
    UIWebView *webView_;
	UIToolbar *toolBar;
    CCTexture2D *texture;
    UIActivityIndicatorView *activityInducator_;
}
+(id) scene:(NSInteger)Page;
-(void)pressPage:(id)sender;
-(void)webBrowser:(NSString *)urltext;
@end
