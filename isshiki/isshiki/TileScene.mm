//
//  TileScene.m
//  Test_box2d
//
//  Created by soei-ima on 11/07/31.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TileScene.h"

NSInteger page3;

@implementation TileScene
+(id) scene:(NSInteger)Page{
    page3 =Page;
    CCScene *scene = [CCScene node];
    TileScene *layer = [TileScene node];
    [scene addChild: layer];
    return scene;
}

-(id) init{
	if( (self=[super init])) {
        
        
        self.isTouchEnabled = YES;

        
        if(page3 == 2){
            [SimpleAudioEngine sharedEngine].effectsVolume = 1.0;
            [[SimpleAudioEngine sharedEngine] preloadEffect:@"title.caf"];
        }
        
        [self pagekirikae];
        
        
    }
    return self;
}



//設定用のファイルパス-------------------------------------------------------------------
- (NSString *)dataFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return [documentsDirectory stringByAppendingPathComponent:@"settei.plist"];
}


-(void)pagekirikae{
    if (page3 == 0) {
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"setumei1.pvr.gz"];
        CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,1024,768)];
        
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack];
        
        CCMenuItemImage *nextBTN = [CCMenuItemImage 
                                       itemWithNormalImage:@"title_next.pvr.gz" 
                                       selectedImage: @"title_next.pvr.gz"
                                       target:self
                                       selector:@selector(pressTitleMenuItem:)];
        nextBTN.position = ccp(0, -330);
        
        CCMenu *menu = [CCMenu menuWithItems:nextBTN, nil];
        [self addChild:menu];
        
    }else if (page3 == 1) {
        
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"setumei2.pvr.gz"];
        CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,1024,768)];
        
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack];
        CCMenuItemImage *nextBTN = [CCMenuItemImage 
                                       itemWithNormalImage:@"title_next.pvr.gz" 
                                       selectedImage: @"title_next.pvr.gz"
                                       target:self
                                       selector:@selector(pressTitleMenuItem:)];
        nextBTN.position = ccp(0, -330);
        
        CCMenu *menu = [CCMenu menuWithItems:nextBTN, nil];
        [self addChild:menu];
        
    }else if(page3 == 2){
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"page0.pvr.gz"];
        CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,1024,768)];
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack];
        
        CCSprite *sprite2 = [CCSprite spriteWithFile:@"title-text.pvr.gz"];
        sprite2.opacity =0.0f;
        sprite2.position = ccp(150+sprite2.contentSize.width/2, 450);
        [self addChild:sprite2];
        
        id Fade = [CCFadeIn actionWithDuration:2.0];
        id fanc = [CCCallFunc actionWithTarget:self selector:@selector(Storystart)];
        id action1 = [CCSequence actions:Fade,fanc,nil];
        [sprite2 runAction:action1];
        
        CCMenuItemImage *systemmenu = [CCMenuItemImage 
                                       itemWithNormalImage:@"system.pvr.gz" 
                                       selectedImage: @"systemOn.png"
                                       target:self
                                       selector:@selector(pressSystemMenuItem:)];
        systemmenu.position = ccp(-450, 330);
        
        CCMenu *menu = [CCMenu menuWithItems:systemmenu, nil];
        [self addChild:menu];
        
    }else if(page3 == 3){
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"page0.pvr.gz"];
        CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,1024,768)];
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack z:-1];
        
        CCSprite *sprite2 = [CCSprite spriteWithFile:@"title-text.pvr.gz"];
        sprite2.position = ccp(150+sprite2.contentSize.width/2, 450);
        [self addChild:sprite2];
        
        id fanc = [CCCallFunc actionWithTarget:self selector:@selector(Storystart)];
        id action1 = [CCSequence actions:fanc,nil];
        [sprite2 runAction:action1];
        
        CCMenuItemImage *systemmenu = [CCMenuItemImage 
                                       itemWithNormalImage:@"system.pvr.gz" 
                                       selectedImage: @"system.pvr.gz"
                                       target:self
                                       selector:@selector(pressSystemMenuItem:)];
        systemmenu.position = ccp(-450, 330);
        
        CCMenu *menu = [CCMenu menuWithItems:systemmenu, nil];
        [self addChild:menu];
        
    }
    
    
}

//画面説明遷移
-(void)pressTitleMenuItem:(id)sender{
    if(page3 < 2){
        NSInteger Page_count = page3;
        Page_count = Page_count+1;
        CCScene *scene = [TileScene scene:Page_count];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
    }
    else if(page3 == 2 || page3 == 3){
        [self setteiRead];
        CCScene *scene = [MainPage scene:19:optionnum[0]:optionnum[1]:optionnum[2]:true];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
    }
}

//物語スタート
-(void)pressSystemMenuItem:(id)sender{
    if(page3 == 2 || page3 == 3){
        if(titleVoice){
            [[SimpleAudioEngine sharedEngine] stopEffect:titleVoice];
        }
        CCScene *scene = [settei scene];
        [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageBackward duration:0.5];
    }
}

-(void)Storystart{
    if(page3 != 3){
       titleVoice = [[SimpleAudioEngine sharedEngine] playEffect:@"title.caf"];
    }
    CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"start.pvr.gz"];
    CCSprite *sprite3 = [CCSprite spriteWithTexture:background];
    sprite3.opacity =0.0f;
    sprite3.position = ccp(512, 150);
    [self addChild:sprite3]; 
    
    id Fade = [CCFadeIn actionWithDuration:1.0];
    id Fade2 = [CCFadeOut actionWithDuration:1.0];
    id action1 = [CCSequence actions:Fade,[CCDelayTime actionWithDuration:2.0f],Fade2,nil];
    id rep = [CCRepeatForever actionWithAction: [[action1 copy] autorelease]];
    [sprite3 runAction:rep];
    start = true;    
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if(start){
        [self pressTitleMenuItem:nil];
    }
}

//設定ファイル読み込み
-(void)setteiRead{
	NSString *filePath = [self dataFilePath];
	if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
		NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (int i = 0; i<3; i++) {
            NSString *Settei  = [[NSString alloc] init];
            Settei = [array objectAtIndex:i];
            optionnum[i] = [Settei intValue];

        }
        [array release];
	}
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	[super dealloc];
}
@end