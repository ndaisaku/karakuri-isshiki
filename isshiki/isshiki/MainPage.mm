//
//  MainPage.mm
//  Test_box2d
//
//  Created by mac on 11/07/24.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "MainPage.h"
#import "SceneManager.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
#define PTM_RATIO 32
// enums that will be used as tags
enum {
	kTagTileMap = 1,
	kTagBatchNode = 1,
	kTagAnimation1 = 1,
    kTagSprite = 1,
};

NSInteger page;
NSInteger optionnum2[3];

//BG用戻り判断
BOOL next;

// MainPage implementation
@implementation MainPage
+(id) scene:(NSInteger)Page:(NSInteger)sette1:(NSInteger)sette2:(NSInteger)sette3:(BOOL)nextPage;
{
    next = nextPage;
    page = Page;
    optionnum2[0] = sette1;
    optionnum2[1] = sette2;
    optionnum2[2] = sette3;
    
	CCScene *scene = [CCScene node];
    MainPage *layer = [MainPage node];
    [scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        
        CCLOG(@"%d",page);
        [SoundPlayer narraLoad:optionnum2[0]:page];
        
        //SEの設定
        [SimpleAudioEngine sharedEngine].effectsVolume = 1;
        
        //BGMの読み込み
        if(page == 1 && next){
            [SoundPlayer bgmLoad:optionnum2[1]];
            [SoundPlayer sePlay:optionnum2[1]:page];
        }
        
        
        //タッチの判定
        touche =false;
        
        //鐘の音制御
        kane = false;
        kanecount =0;
        
        //CCLOG(@"%@:%@",NSStringFromSelector(_cmd),self);
        
        
        
		// タッチできるかどうか
		self.isTouchEnabled = YES;
		
		// アクセレータの有無
        self.isAccelerometerEnabled = NO;
        
        //scene14で使用する焚き火判定
        if(page == 14){
            SoundContorl_takibi = false;
        }else if(page == 16){
            maouPower =6;
        }
		
		CGSize screenSize = [CCDirector sharedDirector].winSize;
		//CCLOG(@"Screen width %0.2f screen height %0.2f",screenSize.width,screenSize.height);
		
		//重力の設定.
		b2Vec2 gravity;
        if(page == 8){
            gravity.Set(0.0f, -50.0f);
            
        }else{
            gravity.Set(0.0f, -50.0f);
        }
		
		
		// Construct a world object, which will hold and simulate the rigid bodies.
		world = new b2World(gravity);
        
		world->SetContinuousPhysics(true);
		/*
         // Debug Draw functions
         m_debugDraw = new GLESDebugDraw( PTM_RATIO );
         world->SetDebugDraw(m_debugDraw);
         uint32 flags = 0;
         flags += b2Draw::e_shapeBit;
         flags += b2Draw::e_jointBit;
         flags += b2Draw::e_aabbBit;
         //flags += b2DebugDraw::e_pairBit;
         //flags += b2DebugDraw::e_centerOfMassBit;
         m_debugDraw->SetFlags(flags);
         */
        
        
        
		// Define the ground body.
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(0, 0); // bottom-left corner
		
		// Call the body factory which allocates memory for the ground body
		// from a pool and creates the ground box shape (also from a pool).
		// The body is also added to the world.
		groundBody = world->CreateBody(&groundBodyDef);
		
		// Define the ground box shape.
		b2EdgeShape groundBox;		
        
		// bottom
		groundBox.Set(b2Vec2(0,0), b2Vec2(screenSize.width/PTM_RATIO,0));
		groundBody->CreateFixture(&groundBox,0);
		
		// top
		groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO));
		groundBody->CreateFixture(&groundBox,0);
		
		// left
		groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(0,0));
		groundBody->CreateFixture(&groundBox,0);
		
		// right
		groundBox.Set(b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,0));
		groundBody->CreateFixture(&groundBox,0);
		
		
		//Set up sprite
        NSString *path =[[NSString alloc] init];
		path = [NSString stringWithFormat:@"page%d.pvr.gz",page];
        texture[page] = [[CCTextureCache sharedTextureCache] addImage:path];
        spriteBack = [CCSprite spriteWithTexture:texture[page] rect:CGRectMake(0,0,1024,768)];
        
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack z:-1];
        
        
        
        
        //からくりマーク
        if(page == 2 || page == 8 || page == 14 || page == 16 || page == 19 || page==22){
            NSString *path2 =[[NSString alloc] init];
            path2 = [NSString stringWithFormat:@"karakuri.png"];
            CCTexture2D *texture2 = [[CCTextureCache sharedTextureCache] addImage:path2];
            CCSprite *mark = [CCSprite spriteWithTexture:texture2];
            mark.position = ccp(122,722);
            [self addChild: mark z:10];
        }
        
        if(optionnum2[2] == 0){
            //Plistから読み込み
            
            
            NSDictionary * dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"]];
            NSArray *components = [[NSArray alloc] init];
            components = [dictionary allKeys];
            
            NSArray *sorted = [[NSArray alloc] init];
            sorted = [components sortedArrayUsingSelector:@selector(compare:)];
            
            NSArray *Page2 = [[NSArray alloc] init];
            Page2 = sorted;
            
            NSString *title = [[NSString alloc] init];
            title = [Page2 objectAtIndex:page-1];
            
            NSString *Story_text = [[NSString alloc] init];
            Story_text = [dictionary objectForKey:title];
            
            //テキストの背景
            textBG_Sprite = [CCSprite spriteWithFile:@"text_field.pvr.gz"];
            textBG_Sprite.position = ccp(512, 0-textBG_Sprite.contentSize.height);
            [self addChild:textBG_Sprite z:11];
            
            //文章読み込み
            CCLabelTTF *label = [CCLabelTTF labelWithString:Story_text
                                                   fontName:@"HiraKakuProN-W3"
                                                   fontSize:20];
            label.dimensions = CGSizeMake(850, 145);
            label.color = ccc3(255, 255, 255);
            label.position =  ccp(520, 115);
            [textBG_Sprite addChild:label z:3];
            
            id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, 110)];
            id func =[CCCallFunc actionWithTarget:self selector:@selector(Playnarration)];
            id ease1 =[CCEaseSineOut actionWithAction:move1];
            id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],ease1,nil];
            id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],func,nil];
            
            [self runAction:action2];
            [textBG_Sprite runAction:action1];
            
            
            //ページめくり+homeボタンの生成
            CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                          itemWithNormalImage:@"nextpage.pvr.gz" 
                                          selectedImage: @"nextpage.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem1.position = ccp(512-maryItem1.contentSize.width/2, -265);
            maryItem1.tag =1;
            
            CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                          itemWithNormalImage:@"prepage.pvr.gz" 
                                          selectedImage: @"prepage.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem2.position = ccp(-512+maryItem2.contentSize.width/2, -265);
            maryItem2.tag =2;
            
            maryItem3 = [CCMenuItemImage 
                         itemWithNormalImage:@"arrow_down.pvr.gz" 
                         selectedImage: @"arrow_down.pvr.gz"
                         target:self
                         selector:@selector(pressMenuItem:)];
            maryItem3.position = ccp(330+maryItem3.contentSize.width/2, -142);
            maryItem3.tag =3;
            
            maryItem4 = [CCMenuItemImage 
                         itemWithNormalImage:@"arrow_up.pvr.gz" 
                         selectedImage: @"arrow_up.pvr.gz"
                         target:self
                         selector:@selector(pressMenuItem:)];
            maryItem4.position = ccp(330+maryItem4.contentSize.width/2, -142);
            maryItem4.tag =4;
            maryItem4.visible = false;
            
            
            
            CCMenu *menu = [CCMenu menuWithItems:maryItem1, maryItem2,maryItem3,maryItem4,nil];
            [textBG_Sprite addChild:menu];
            
        }else{
            CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                          itemWithNormalImage:@"arrow_box_left.pvr.gz" 
                                          selectedImage: @"arrow_box_left.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem1.position = ccp(-128+maryItem1.contentSize.width/2, maryItem1.contentSize.height/2);
            maryItem1.tag =2;
            
            CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                          itemWithNormalImage:@"arrow_box_right.pvr.gz" 
                                          selectedImage: @"arrow_box_right.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem2.position = ccp(128-maryItem2.contentSize.width/2, maryItem2.contentSize.height/2);
            maryItem2.tag =1;
            CCMenu *menu = [CCMenu menuWithItems:maryItem1, maryItem2,nil];
            menu.position = ccp(512,-100);
            [self addChild:menu z:10];
            
            id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512,0)];
            id ease1 =[CCEaseSineOut actionWithAction:move1];
            
            id func =[CCCallFunc actionWithTarget:self selector:@selector(Playnarration)];
            id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0],ease1,nil];
            id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.8],func,nil];
            
            [self runAction:action2];
            [menu runAction:action1];
        }
        CCMenuItemImage *maryItem5 = [CCMenuItemImage 
                                      itemWithNormalImage:@"home.png" 
                                      selectedImage: @"home.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem5.position = ccp(-460, 340);
        maryItem5.tag =5;
        CCMenu *menu2 = [CCMenu menuWithItems:maryItem5,nil];
        [self addChild:menu2 z:10];
        [self PageEffect];
        if (page !=19) {
            [self schedule: @selector(tick:)];
        }
	}
	return self;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[self didReceiveMemoryWarning];
    
	// Release any cached data, images, etc that aren't in use.
}

//メニューボタン処理
-(void)pressMenuItem:(id)sender {
    if([sender tag] == 1){
        if(page < 23){
            if(optionnum2[1] == 0){
                [[SimpleAudioEngine sharedEngine] stopEffect:se];
            }
            if(page == 14 || page == 15 || page == 17){
                [SoundPlayer bgmStop:optionnum2[1]:3];
            }
            
            [SoundPlayer StopNarr:optionnum2[0]:page];
            NSInteger Page_count = page;
            Page_count += 1;
            next = true;
            CCScene *scene = [MainPage scene:Page_count:optionnum2[0]:optionnum2[1]:optionnum2[2]:next];
            [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
            
        }else{
            [SoundPlayer bgmStop:optionnum2[1]:7];
            [SoundPlayer StopNarr:optionnum2[0]:page];
            [SoundPlayer countReset];
            CCScene *scene = [finish scene:1];
            [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
        }
        
    }else if([sender tag] == 2){
        if(page > 1){
            if(optionnum2[1] == 0){
                [[SimpleAudioEngine sharedEngine] stopEffect:se];
            }
            [SoundPlayer StopNarr:optionnum2[0]:page];
            NSInteger Page_count = page;
            Page_count = Page_count-1;
            CCLOG(@"page%d",Page_count);
            next = false;
            CCScene *scene = [MainPage scene:Page_count:optionnum2[0]:optionnum2[1]:optionnum2[2]:next];
            [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageBackward duration:0.5];
        }
    }else if([sender tag] == 3){
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, -75)];
        CCEaseExponentialOut *ease1 =[CCEaseExponentialOut actionWithAction:move1];
        [textBG_Sprite runAction:ease1];
        maryItem3.visible =false;
        maryItem4.visible =true;
        
    }else if([sender tag] == 4){
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, 110)];
        CCEaseExponentialOut *ease1 =[CCEaseExponentialOut actionWithAction:move1];
        [textBG_Sprite runAction:ease1];
        maryItem3.visible =true;
        maryItem4.visible =false;
        
    }else if([sender tag] == 5){
        UIAlertView *alertView = [[UIAlertView alloc] 
                                  initWithTitle:@"タイトルへ戻りますか？"
                                  message:nil                             
                                  delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"はい",@"いいえ",nil];
        [alertView show];
        [alertView release];
    }
    
}
//タイトルへ戻るアラート
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [SoundPlayer StopNarr:optionnum2[0]:page];
            [SoundPlayer bgmStop2:optionnum2[1]];
            if(optionnum2[1] == 0){
                [[SimpleAudioEngine sharedEngine] stopEffect:se];
            }
            CCScene *scene = [TileScene scene:3];
            [CCTransitionHelper replaceScene:scene transitionType:TransitionTypePageForward duration:0.5];
            break;
    }
}

//シーン17
-(void) addNewSpriteWithJoint:(CGPoint)p{
    //動的ボディの定義
    CGSize winSize = [[CCDirector sharedDirector] winSize];

    b2PolygonShape dynamicBox[4];
    b2FixtureDef fixtureDef;
    
	b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    b2Body* ground = world->CreateBody(&bodyDef);
    
    //ボックスシェイプを定義
    float boxhsize = Movie_Sprite[0].contentSize.height / PTM_RATIO*0.4f;
    float boxwsize = Movie_Sprite[0].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize2 = Movie_Sprite[1].contentSize.height / PTM_RATIO*0.32f;
    float boxwsize2 = Movie_Sprite[1].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize3 = Movie_Sprite[2].contentSize.height / PTM_RATIO*0.32f;
    float boxwsize3 = Movie_Sprite[2].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize4 = Movie_Sprite[3].contentSize.height / PTM_RATIO*0.32f;
    float boxwsize4 = Movie_Sprite[3].contentSize.width / PTM_RATIO*0.5f;
    
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	bodyDef.userData = Movie_Sprite[0];
	m_bodies[0] = world->CreateBody(&bodyDef);
	dynamicBox[0].SetAsBox(boxwsize, boxhsize);
    
	bodyDef.position.Set(512/PTM_RATIO, 448/PTM_RATIO);
    bodyDef.userData = Movie_Sprite[1];
    m_bodies[1] = world->CreateBody(&bodyDef);
    dynamicBox[1].SetAsBox(boxwsize2, boxhsize2);
    
    bodyDef.position.Set(512/PTM_RATIO, 390/PTM_RATIO);
    bodyDef.userData = Movie_Sprite[2];
    m_bodies[2] = world->CreateBody(&bodyDef);
    dynamicBox[2].SetAsBox(boxwsize3, boxhsize3);
    
    bodyDef.position.Set(512/PTM_RATIO, 285/PTM_RATIO);
    bodyDef.userData = Movie_Sprite[3];
    m_bodies[3] = world->CreateBody(&bodyDef);
    dynamicBox[3].SetAsBox(boxwsize4, boxhsize4);
    
    b2DistanceJointDef jd;
    b2Vec2 p1, p2, d;
    
    jd.frequencyHz = 40.0f;
    jd.dampingRatio = 8.0f;
    
    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p.x/PTM_RATIO-200/PTM_RATIO, p.y/PTM_RATIO+boxhsize+200/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize+50/PTM_RATIO, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[0] = world->CreateJoint(&jd);
    
    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p.x/PTM_RATIO+200/PTM_RATIO, p.y/PTM_RATIO+boxhsize+200/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize-50/PTM_RATIO, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[5] = world->CreateJoint(&jd);    
    
    jd.bodyA = m_bodies[0];
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(0, -boxhsize/PTM_RATIO);
    jd.localAnchorB.Set(0, boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[1] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[1];
    jd.bodyB = m_bodies[2];
    jd.localAnchorA.Set(0, -boxhsize2);
    jd.localAnchorB.Set(0, boxhsize3);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[2] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[2];
    jd.bodyB = m_bodies[3];
    jd.localAnchorA.Set(0, -boxhsize3);
    jd.localAnchorB.Set(0, boxhsize4);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[3] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[3];
    jd.bodyB = groundBody;
    jd.localAnchorA.Set(0, -boxhsize4);
    jd.localAnchorB.Set((winSize.width/2)/PTM_RATIO, 100/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[4] = world->CreateJoint(&jd);
    
    
    // Define the dynamic body fixture.
    for(int32 i=0; i<4; i++){
        fixtureDef.shape = &dynamicBox[i];	
        fixtureDef.density =10.0f;
        fixtureDef.friction = 10.0f;
        fixtureDef.restitution = 0.1f;
        _paddleFixture[i] = m_bodies[i]->CreateFixture(&fixtureDef);
        
    }
    
    b2FixtureDef fixtureDef2;
	fixtureDef2.shape = &dynamicBox[0];
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
    ground->CreateFixture(&fixtureDef2);

    
    touche = true;
}


#pragma mark -
#pragma mark シーン19:吊り人形
-(void) addNewSpriteWithJoint2{
    //動的ボディの定義
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    b2PolygonShape dynamicBox[7];
    b2FixtureDef fixtureDef;
    
	b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    b2Body* ground = world->CreateBody(&bodyDef);
    

    
	bodyDef.type = b2_dynamicBody;
    
    //ボックスシェイプを定義
    float boxhsize = P19_Movie_Sprite[0].contentSize.height / PTM_RATIO*0.5f;
    float boxwsize = P19_Movie_Sprite[0].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize2 = P19_Movie_Sprite[1].contentSize.height / PTM_RATIO*0.33f;
    float boxwsize2 = P19_Movie_Sprite[1].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize3 = P19_Movie_Sprite[2].contentSize.height / PTM_RATIO*0.4f;
    float boxwsize3 = P19_Movie_Sprite[2].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize4 = P19_Movie_Sprite[3].contentSize.height / PTM_RATIO*0.4f;
    float boxwsize4 = P19_Movie_Sprite[3].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize5 = P19_Movie_Sprite[4].contentSize.height / PTM_RATIO*0.33f;
    float boxwsize5 = P19_Movie_Sprite[4].contentSize.width / PTM_RATIO*0.5f;
    float boxhsize6 = P19_Movie_Sprite[5].contentSize.height / PTM_RATIO*0.33f;
    float boxwsize6 = P19_Movie_Sprite[5].contentSize.width / PTM_RATIO*0.5f;
    
    
	bodyDef.position.Set(670/PTM_RATIO, 590/PTM_RATIO);
    bodyDef.userData = P19_Movie_Sprite[0];
	m_bodies[0] = world->CreateBody(&bodyDef);
	dynamicBox[0].SetAsBox(boxwsize, boxhsize);
    
	bodyDef.position.Set(750/PTM_RATIO, 418/PTM_RATIO);
    bodyDef.userData = P19_Movie_Sprite[1];
    m_bodies[1] = world->CreateBody(&bodyDef);
    dynamicBox[1].SetAsBox(boxwsize2, boxhsize2);
    
    bodyDef.position.Set(695/PTM_RATIO, 210/PTM_RATIO);
    bodyDef.userData = P19_Movie_Sprite[2];
    m_bodies[2] = world->CreateBody(&bodyDef);
    dynamicBox[2].SetAsBox(boxwsize3, boxhsize3);
    
    bodyDef.position.Set(395/PTM_RATIO, 580/PTM_RATIO);
    bodyDef.userData = P19_Movie_Sprite[3];
    m_bodies[3] = world->CreateBody(&bodyDef);
    dynamicBox[3].SetAsBox(boxwsize4, boxhsize4);
    
    bodyDef.position.Set(365/PTM_RATIO, 428/PTM_RATIO);
    bodyDef.userData = P19_Movie_Sprite[4];
    m_bodies[4] = world->CreateBody(&bodyDef);
    dynamicBox[4].SetAsBox(boxwsize5, boxhsize5);
    
    bodyDef.position.Set(355/PTM_RATIO, 220/PTM_RATIO);
    bodyDef.userData = P19_Movie_Sprite[5];
    m_bodies[5] = world->CreateBody(&bodyDef);
    dynamicBox[5].SetAsBox(boxwsize6, boxhsize6);
    
    b2DistanceJointDef jd;
    b2Vec2 p1, p2, d;
    
    jd.frequencyHz = 8.0f;
    jd.dampingRatio = 1.0f;
    
    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(670/PTM_RATIO-100/PTM_RATIO, 580/PTM_RATIO+boxhsize+100/PTM_RATIO);
    jd.localAnchorB.Set(30/PTM_RATIO, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[0] = world->CreateJoint(&jd);
    
    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(670/PTM_RATIO+100/PTM_RATIO, 580/PTM_RATIO+boxhsize+100/PTM_RATIO);
    jd.localAnchorB.Set(30/PTM_RATIO, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[1] = world->CreateJoint(&jd);
    
    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(670/PTM_RATIO+200/PTM_RATIO, 580/PTM_RATIO-boxhsize+100/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize, 0);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[10] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[0];
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(40/PTM_RATIO, -boxhsize+40/PTM_RATIO);
    jd.localAnchorB.Set(0, boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = 0.1;
    m_joints[2] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[1];
    jd.bodyB = m_bodies[2];
    jd.localAnchorA.Set(0, -boxhsize2-20/PTM_RATIO);
    jd.localAnchorB.Set(0, boxhsize3);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = 0.3;
    m_joints[3] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[2];
    jd.bodyB = groundBody;
    jd.localAnchorA.Set(0, -boxhsize3);
    jd.localAnchorB.Set(695/PTM_RATIO, 210/PTM_RATIO-boxhsize3-100/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[4] = world->CreateJoint(&jd);
    
    //左の男
    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[3];
    jd.localAnchorA.Set(395/PTM_RATIO-100/PTM_RATIO, 580/PTM_RATIO+boxhsize+100/PTM_RATIO);
    jd.localAnchorB.Set(-30/PTM_RATIO, boxhsize4);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[5] = world->CreateJoint(&jd);
    
    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[3];
    jd.localAnchorA.Set(405/PTM_RATIO+100/PTM_RATIO, 580/PTM_RATIO+boxhsize+100/PTM_RATIO);
    jd.localAnchorB.Set(-30/PTM_RATIO, boxhsize4);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[6] = world->CreateJoint(&jd);

    jd.bodyA = groundBody;
    jd.bodyB = m_bodies[3];
    jd.localAnchorA.Set(405/PTM_RATIO-200/PTM_RATIO, 590/PTM_RATIO-boxhsize+100/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize4, 0);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[11] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[3];
    jd.bodyB = m_bodies[4];
    jd.localAnchorA.Set(-20/PTM_RATIO, -boxhsize4+40/PTM_RATIO);
    jd.localAnchorB.Set(40/PTM_RATIO, boxhsize5);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = 0.1;
    m_joints[7] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[4];
    jd.bodyB = m_bodies[5];
    jd.localAnchorA.Set(-40/PTM_RATIO, -boxhsize5);
    jd.localAnchorB.Set(0, boxhsize6);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = 0.3;
    m_joints[8] = world->CreateJoint(&jd);
    
    jd.bodyA = m_bodies[5];
    jd.bodyB = groundBody;
    jd.localAnchorA.Set(0, -boxhsize6);
    jd.localAnchorB.Set(355/PTM_RATIO, 220/PTM_RATIO-boxhsize6-200/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[9] = world->CreateJoint(&jd);
    
    
    // Define the dynamic body fixture.
    for(int32 i=0; i<6; i++){
        fixtureDef.shape = &dynamicBox[i];	
        fixtureDef.density =5.0f;
        fixtureDef.friction = 30.0f;
        fixtureDef.restitution = 0.4f;
        _paddleFixture[i] = m_bodies[i]->CreateFixture(&fixtureDef);
        
    }
    
    b2FixtureDef fixtureDef2;
	fixtureDef2.shape = &dynamicBox[0];
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
    ground->CreateFixture(&fixtureDef2);
    
    touche = true;
}

#pragma mark -
#pragma mark シーン22:殿様転がり
-(void) addNewSpriteWithCoords
{
    
    //動的ボディの定義
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
    
	bodyDef.position.Set(512/PTM_RATIO, 300/PTM_RATIO);
	bodyDef.userData = Movie_Sprite[0];
	body = world->CreateBody(&bodyDef);
	
    //ボックスシェイプを定義
	b2CircleShape circleShape;
    float boxwsize =  Movie_Sprite[0].contentSize.width / PTM_RATIO;
    circleShape.m_radius = boxwsize*0.5;

	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circleShape;	
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 1.0f;
    fixtureDef.restitution = 0.5f;
	_paddleFixture[0] = body->CreateFixture(&fixtureDef);
}

#pragma mark -
#pragma mark draw
-(void) draw
{
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();
	
	kmGLPopMatrix();
    
    
}

#pragma mark -
#pragma mark tick

-(void) tick: (ccTime) dt
{  
   	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
	
	//Iterate over the bodies in the physics world
           for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
        {
            if (b->GetUserData() != NULL) {
                //Synchronize the AtlasSprites position and rotation with the corresponding body
                CCSprite *myActor = (CCSprite*)b->GetUserData();
                myActor.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
                myActor.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
            }	
    }
    
    //TODO: 鐘衝突音
    if(page == 19){
        CGRect projectileRect = CGRectMake(
                                           P19_Movie_Sprite[1].position.x -(P19_Movie_Sprite[1].contentSize.width/2), 
                                           P19_Movie_Sprite[1].position.y -(P19_Movie_Sprite[1].contentSize.height/2), 
                                           P19_Movie_Sprite[1].contentSize.width, 
                                           P19_Movie_Sprite[1].contentSize.height);
        CGRect targetRect = CGRectMake(
                                       P19_Movie_Sprite[4].position.x -(P19_Movie_Sprite[4].contentSize.width/2), 
                                       P19_Movie_Sprite[4].position.y -(P19_Movie_Sprite[4].contentSize.height/2), 
                                       P19_Movie_Sprite[4].contentSize.width+15, 
                                       P19_Movie_Sprite[4].contentSize.height);
        
        if(CGRectIntersectsRect(projectileRect, targetRect)){
            if(optionnum2[1] == 0){
                se = [[SimpleAudioEngine sharedEngine] playEffect:@"kenka.mp3"];
            }
                
        } 
    }
}



#pragma mark -
#pragma mark 各ページのエフェクト

-(void)PageEffect{
    if(page == 1){
        //TODO: BGM読み込み
        [SoundPlayer bgmPlay:optionnum2[1]:0];
        
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene1.plist" textureFilename:@"head.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"head.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"scene1_head1.png"];
        sprite.anchorPoint = ccp(0.25, 0.2);
        sprite.position = ccp( 378, 384 );
        
        CCSprite * sprite2 = [CCSprite spriteWithSpriteFrameName:@"scene1_head2.png"];
        sprite2.anchorPoint = ccp(0.3, 0.1);
        sprite2.position = ccp( 154, 366 );
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"body.png"];
        sprite3.position = ccp( 282, 360 );
        
        [self addChild:sprite];
        [self addChild:sprite2];
        [self addChild:sprite3];
        
        id Rote = [CCRotateBy actionWithDuration: 0.3f angle:10]; 
        id Rote2 = [Rote reverse];
        id action = [CCSequence actions:Rote,Rote2,Rote,Rote2,[CCDelayTime actionWithDuration:1.5f],nil];
        id rep = [CCRepeatForever actionWithAction:[[action copy] autorelease]];
        
        id Rote3 = [CCRotateBy actionWithDuration: 0.3f angle:10]; 
        id Rote4 = [Rote reverse];
        id action2 = [CCSequence actions:Rote3,Rote4,Rote3,Rote4,[CCDelayTime actionWithDuration:1.5f],nil];
        id rep2 = [CCRepeatForever actionWithAction:[[action2 copy] autorelease]];
        
        [sprite runAction:rep];
        [sprite2 runAction:rep2];        
        
    }else if(page == 2){
        
        touche =true;
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene2.plist" textureFilename:@"hand.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hand.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"te.png"];
        Movie_Sprite[0].anchorPoint = ccp(0.0, 0.15);
        Movie_Sprite[0].position = ccp( 500, 75 );
        [self addChild:Movie_Sprite[0] z:3];
        
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"te2.png"];
        Movie_Sprite[1].anchorPoint = ccp(0.0, 0.15);
        Movie_Sprite[1].position = ccp( 155, 75 );
        [self addChild:Movie_Sprite[1] z:4];
        
    }else if(page == 3){
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"hito.plist" textureFilename:@"hito.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hito.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"hito.png"];
        sprite.position = ccp( 500, 235 );
        [self addChild:sprite];
        
        id Fade = [CCFadeOut actionWithDuration:0.5];
        id Scale = [CCScaleBy actionWithDuration:0 scale:0.6f];
        id Fade2 = [CCFadeIn actionWithDuration:0.5];
        id Scale2 = [CCScaleBy actionWithDuration:0 scale:0.2f];
        
        id Move = [CCMoveTo actionWithDuration:0 position:ccp(490,300)];
        id Move2 = [CCMoveTo actionWithDuration:0 position:ccp(490,320)];
        
        id action = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],Fade,Scale,Move,Fade2,[CCDelayTime actionWithDuration:1.0f],Fade,Scale2,Move2,Fade2,nil];
        [sprite runAction:action];
        
    }else if(page == 4){
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene4.plist" textureFilename:@"scene4.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene4.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"kyoudai.png"];
        sprite3.position = ccp( 1350, 355 );
        [self addChild:sprite3];
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"titi.png"];
        sprite.position = ccp( 1024, 305 );
        [self addChild:sprite];
        
        CCSprite * sprite2 = [CCSprite spriteWithSpriteFrameName:@"oka.png"];
        sprite2.position = ccp( 1224, 318 );
        [self addChild:sprite2];
        
        id Move = [CCMoveTo actionWithDuration:1.0 position:ccp(500,305)];
        id Move2 = [CCMoveTo actionWithDuration:1.4 position:ccp(700,318)];
        id Move3 = [CCMoveTo actionWithDuration:1.8 position:ccp(850,355)];
        
        id ease = [CCEaseSineOut actionWithAction:Move];
        id ease2 = [CCEaseSineOut actionWithAction:Move2];
        id ease3 = [CCEaseSineOut actionWithAction:Move3];
        
        [sprite runAction:ease];
        [sprite2 runAction:ease2];
        [sprite3 runAction:ease3];
        
    }else if(page == 5){
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene5.plist" textureFilename:@"scene5.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene5.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"fune.png"];
        sprite.position = ccp( 449, 409 );
        [self addChild:sprite];
        
        CCSprite * sprite2 = [CCSprite spriteWithSpriteFrameName:@"nami.png"];
        sprite2.position = ccp( 436, 350 );
        [self addChild:sprite2];
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"fuufu.png"];
        sprite3.position = ccp( 725, 280 );
        [self addChild:sprite3];
        
        CCSprite * sprite4 = [CCSprite spriteWithSpriteFrameName:@"hikari.png"];
        sprite4.position = ccp( 515, 358 );
        [self addChild:sprite4];
        
        id Move = [CCMoveBy actionWithDuration:2.0 position:ccp(0,10)];
        id ease = [CCEaseSineOut actionWithAction:Move];
        id rev = [ease reverse];
        id ease2 = [CCEaseSineOut actionWithAction:rev];
        id action = [CCSequence actions:Move,ease2,nil];
        id rep = [CCRepeatForever actionWithAction:[[action copy] autorelease]];
        [sprite2 runAction:rep];
        
        
        
        id Move2 = [CCMoveBy actionWithDuration:2.0 position:ccp(0,8)];
        id ease3 = [CCEaseSineOut actionWithAction:Move2];
        id rev2 = [ease3 reverse];
        id ease4 = [CCEaseSineOut actionWithAction:rev2];
        
        id action2 = [CCSequence actions:ease3,ease4,nil];
        
        id rep2 = [CCRepeatForever actionWithAction:[[action2 copy] autorelease]];
        [sprite runAction:rep2];
        
        id Fadein = [CCFadeIn actionWithDuration:1.0];
        id FadeOut = [CCFadeOut actionWithDuration:1.0];
        id action1 = [CCSequence actions:FadeOut,Fadein,nil];
        id rep3 = [CCRepeatForever actionWithAction:[[action1 copy] autorelease]];
        [sprite4 runAction:rep3];
        
    }else if(page == 6){
        
        //BGM読み込み
        if(!next){
            [SoundPlayer bgmStop:optionnum2[1]:5];
            [SoundPlayer bgmPlay:optionnum2[1]:0];
        }
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene6_kumo.plist" textureFilename:@"scene6_kumo.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene6_kumo.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSpriteFrameCache *frameCache2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache2 addSpriteFramesWithFile:@"scene6_nami.plist" textureFilename:@"scene6_nami.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet2 = [CCSpriteBatchNode batchNodeWithFile:@"scene6_nami.pvr.gz"];
        [self addChild:spriteSheet2];
        
        CCSpriteFrameCache *frameCache3 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache3 addSpriteFramesWithFile:@"scene6_other.plist" textureFilename:@"scene6_other.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet3 = [CCSpriteBatchNode batchNodeWithFile:@"scene6_other.pvr.gz"];
        [self addChild:spriteSheet3];
        
        CCSpriteFrameCache *frameCache4 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache4 addSpriteFramesWithFile:@"scene7_frame.plist" textureFilename:@"frame.png"];
        
        CCSpriteBatchNode *spriteSheet4 = [CCSpriteBatchNode batchNodeWithFile:@"frame.png"];
        [self addChild:spriteSheet4];
        
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"kumo1.png"];
        Movie_Sprite[0].position = ccp( 1124, 620 );
        [self addChild:Movie_Sprite[0]];
        
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"kumo2.png"];
        Movie_Sprite[1].position = ccp( 1624, 610 );
        [self addChild:Movie_Sprite[1]];
        
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"kumo3.png"];
        Movie_Sprite[2].position = ccp( 1324, 480 );
        [self addChild:Movie_Sprite[2]];
        
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"kumo4.png"];
        Movie_Sprite[3].position = ccp( 1124, 550 );
        
        
        CCSprite * sprite5 = [CCSprite spriteWithSpriteFrameName:@"nami1.png"];
        sprite5.position = ccp( 515, 100 );
        
        CCSprite * sprite6 = [CCSprite spriteWithSpriteFrameName:@"nami2.png"];
        sprite6.position = ccp( 515, 130 );
        
        CCSprite * sprite7 = [CCSprite spriteWithSpriteFrameName:@"nami3.png"];
        sprite7.position = ccp( 515, 185 );
        
        CCSprite * sprite8 = [CCSprite spriteWithSpriteFrameName:@"nami4.png"];
        sprite8.position = ccp( 515, 160 );
        
        CCSprite *p6_sprite9 = [CCSprite spriteWithSpriteFrameName:@"p6_fune2.png"];
        p6_sprite9.position = ccp( 515, 270 );
        
        CCSprite *p6_sprite10 = [CCSprite spriteWithSpriteFrameName:@"p6_kirakira.png"];
        p6_sprite10.position = ccp( 515, 180 );
        
        CCSprite *p6_sprite11 = [CCSprite spriteWithSpriteFrameName:@"p6_anun.png"];
        p6_sprite11.position = ccp( 850, 180 );
        
        CCSprite *p6_sprite12 = [CCSprite spriteWithSpriteFrameName:@"frame.png"];
        p6_sprite12.position = ccp(p6_sprite12.contentSize.width/2, p6_sprite12.contentSize.height/2 );
        
        
        [self addChild:p6_sprite11];
        [self addChild:Movie_Sprite[3]];
        
        [self addChild:sprite8];
        [self addChild:sprite7];
        [self addChild:p6_sprite9];
        [self addChild:sprite6];
        [self addChild:sprite5];
        [self addChild:p6_sprite10];
        
        id Move = [CCMoveBy actionWithDuration:20.0 position:ccp(-1324,0)];
        id Move2 = [CCMoveBy actionWithDuration:25.0 position:ccp(-1824,0)];
        id Move3 = [CCMoveBy actionWithDuration:22.0 position:ccp(-1524,0)];
        id Move4 = [CCMoveBy actionWithDuration:28.0 position:ccp(-1324,0)];
        
        id func = [CCCallFunc actionWithTarget:self selector:@selector(scene6)];
        id action6 =[CCSequence actions:Move4,func,nil];
        
        id Move5 = [CCMoveBy actionWithDuration:2.0 position:ccp(0,-25)];
        id ease = [CCEaseSineIn actionWithAction:Move5];
        
        id Move6 = [CCMoveBy actionWithDuration:2.5 position:ccp(0,-15)];
        id ease2 = [CCEaseSineIn actionWithAction:Move6];
        
        id Move7 = [CCMoveBy actionWithDuration:3.0 position:ccp(0,10)];
        id ease3 = [CCEaseSineIn actionWithAction:Move7];
        
        id Move8 = [CCMoveBy actionWithDuration:2.6 position:ccp(0,-15)];
        id ease4 = [CCEaseSineIn actionWithAction:Move8];
        
        id Move9 = [CCMoveBy actionWithDuration:5.0 position:ccp(0,150)];
        
        id Fadein = [CCFadeIn actionWithDuration:1.0];
        id FadeOut = [CCFadeOut actionWithDuration:1.0];
        
        id rev = [Move5 reverse];
        id rev2 = [Move6 reverse];
        id rev3 = [Move7 reverse];
        id rev4 = [Move8 reverse];
        
        id ease5 = [CCEaseSineOut actionWithAction:rev];
        id ease6 = [CCEaseSineOut actionWithAction:rev2];
        id ease7 = [CCEaseSineOut actionWithAction:rev3];
        id ease8 = [CCEaseSineOut actionWithAction:Move9];
        
        
        id action = [CCSequence actions:ease,ease5,nil];
        id action2 = [CCSequence actions:ease2,ease6,nil];
        id action3 = [CCSequence actions:ease3,ease7,nil];
        id action4 = [CCSequence actions:ease4,rev4,nil];
        id action5 = [CCSequence actions:[CCDelayTime actionWithDuration:5.0f],ease8,nil];
        id action7 = [CCSequence actions:FadeOut,Fadein,nil];
        
        id rep2 = [CCRepeatForever actionWithAction:[[action copy] autorelease]];
        id rep3 = [CCRepeatForever actionWithAction:[[action2 copy] autorelease]];
        id rep4 = [CCRepeatForever actionWithAction:[[action3 copy] autorelease]];
        id rep5 = [CCRepeatForever actionWithAction:[[action4 copy] autorelease]];
        id rep6 = [CCRepeatForever actionWithAction:[[action7 copy] autorelease]];
        
        [Movie_Sprite[0] runAction:Move];
        [Movie_Sprite[1] runAction:Move2];
        [Movie_Sprite[2] runAction:Move3];
        [Movie_Sprite[3] runAction:action6];
        
        [sprite5 runAction:rep2];
        [sprite6 runAction:rep3];
        [sprite7 runAction:rep4];
        [p6_sprite9 runAction:rep5];
        
        [p6_sprite11 runAction:action5];
        [p6_sprite10 runAction:rep6];
        
        [self addChild:p6_sprite12];
        
        
    }else if(page == 7){
        
        //BGM読み込み
        [SoundPlayer bgmStop:optionnum2[1]:0];
        [SoundPlayer bgmPlay:optionnum2[1]:5];
        
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene7_kumo1.plist" textureFilename:@"scene7_kumo1.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene7_kumo1.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSpriteFrameCache *frameCache2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache2 addSpriteFramesWithFile:@"scene7_kumo2.plist" textureFilename:@"scene7_kumo2.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet2 = [CCSpriteBatchNode batchNodeWithFile:@"scene7_kumo2.pvr.gz"];
        [self addChild:spriteSheet2];
        
        CCSpriteFrameCache *frameCache3 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache3 addSpriteFramesWithFile:@"scene7_bro.plist" textureFilename:@"brother.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet3 = [CCSpriteBatchNode batchNodeWithFile:@"brother.pvr.gz"];
        [self addChild:spriteSheet3];
        
        CCSpriteFrameCache *frameCache4 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache4 addSpriteFramesWithFile:@"scene7_karasu.plist" textureFilename:@"scene7_karasu.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet4 = [CCSpriteBatchNode batchNodeWithFile:@"scene7_karasu.pvr.gz"];
        [self addChild:spriteSheet4];
        
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"frame.png"];
        sprite.position = ccp( sprite.contentSize.width/2, sprite.contentSize.height/2 );
        
        CCSprite * sprite2 = [CCSprite spriteWithSpriteFrameName:@"kyoudai.png"];
        sprite2.position = ccp( 515, -160 );
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"scene7_kumo1.png"];
        sprite3.position = ccp( 65, 100 );
        
        CCSprite * sprite4 = [CCSprite spriteWithSpriteFrameName:@"scene7_kumo2.png"];
        sprite4.position = ccp( 1024, 210 );
        
        CCSprite * sprite5 = [CCSprite spriteWithSpriteFrameName:@"scene7_kumo3.png"];
        sprite5.position = ccp( 0, 380 );
        
        CCSprite * sprite6 = [CCSprite spriteWithSpriteFrameName:@"scene7_kumo4.png"];
        sprite6.position = ccp( 1024, 410 );
        
        CCSprite * sprite7 = [CCSprite spriteWithSpriteFrameName:@"scene7_karasu.png"];
        sprite7.position = ccp( 380, 500 );
        sprite7.opacity  = 0.0;
        
        
        [self addChild:sprite4];
        [self addChild:sprite3];
        [self addChild:sprite5];
        [self addChild:sprite6];
        [self addChild:sprite7];
        [self addChild:sprite2];
        
        [self addChild:sprite];
        
        id Move = [CCMoveBy actionWithDuration:1.0 position:ccp(0,390)];
        id ease = [CCEaseSineOut actionWithAction:Move];
        id action = [CCSequence actions:[CCDelayTime actionWithDuration:2.0], ease, nil];
        
        id Move2 = [CCMoveBy actionWithDuration:2.2 position:ccp(300,175)];
        id ease2 = [CCEaseSineOut actionWithAction:Move2];
        
        id Move3 = [CCMoveBy actionWithDuration:2.5 position:ccp(-(1024-765),100)];
        id ease3 = [CCEaseSineOut actionWithAction:Move3];
        
        id Move4 = [CCMoveBy actionWithDuration:2.8 position:ccp(305,100)];
        id ease4 = [CCEaseSineOut actionWithAction:Move4];
        
        id Move5 = [CCMoveBy actionWithDuration:3.0 position:ccp(-(1024-635),100)];
        id ease5 = [CCEaseSineOut actionWithAction:Move5];
        
        id Move6 = [CCMoveTo actionWithDuration:3.0 position:ccp(300,580)];
        id ease6 = [CCEaseSineOut actionWithAction:Move6];
        id Fadein = [CCFadeIn actionWithDuration:1.0];
        id Action2 = [CCSpawn actions:ease6,Fadein,nil];
        id Action3 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],Action2,nil];
        [sprite6 runAction:ease5];
        [sprite5 runAction:ease4];
        [sprite4 runAction:ease3];
        [sprite3 runAction:ease2];
        [sprite2 runAction:action];
        [sprite7 runAction:Action3];
        
    }else if(page == 8){
        
        //BGM読み込み
        if(!next){
            [SoundPlayer bgmStop:optionnum2[1]:4];
            [SoundPlayer bgmPlay:optionnum2[1]:5];
        }
        
        [self preloadParticleEffect:@"scene8_particle.plist"];
        
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene8_kaijyu.plist" textureFilename:@"scene8_kaijyu.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene8_kaijyu.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu4.png"];
        [self addChild:Movie_Sprite[3]];
        
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu3.png"];
        [self addChild:Movie_Sprite[2]];
        
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu2.png"];
        [self addChild:Movie_Sprite[1]];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu1.png"];
        [self addChild:Movie_Sprite[0]];        
        
        CGPoint location = CGPointMake(512, 600);
        
        particles[0] = [CCParticleSystemQuad particleWithFile:@"scene8_particle.plist"];
        [self addChild:particles[0]];
        
        [self  addNewSpriteWithJoint:location];
        
    }else if(page == 9){
        
        if(next){
            [SoundPlayer bgmStop:optionnum2[1]:5];
            [SoundPlayer bgmPlay:optionnum2[1]:4];
        }
        
        [self preloadParticleEffect:@"scene9_p.plist"];
        
        CCSpriteFrameCache *frameCache2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache2 addSpriteFramesWithFile:@"scene9_kaminari.plist" textureFilename:@"kaminari.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet2 = [CCSpriteBatchNode batchNodeWithFile:@"kaminari.pvr.gz"];
        [self addChild:spriteSheet2];
        
        CCSpriteFrameCache *frameCache3 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache3 addSpriteFramesWithFile:@"scene9_other.plist" textureFilename:@"scene9_other.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet3 = [CCSpriteBatchNode batchNodeWithFile:@"scene9_other.pvr.gz"];
        [self addChild:spriteSheet3];
        
        CCSpriteFrameCache *frameCache4 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache4 addSpriteFramesWithFile:@"scene9_ame.plist" textureFilename:@"scene9_ame.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet4 = [CCSpriteBatchNode batchNodeWithFile:@"scene9_ame.pvr.gz"];
        [self addChild:spriteSheet4];
        
        CCSpriteFrameCache *frameCache5 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache5 addSpriteFramesWithFile:@"scene9_kaibutu.plist" textureFilename:@"scene9_kaibutu.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet5 = [CCSpriteBatchNode batchNodeWithFile:@"scene9_kaibutu.pvr.gz"];
        [self addChild:spriteSheet5];
        
        CCSprite * sprite10 = [CCSprite spriteWithSpriteFrameName:@"scene9_kaibutu1.png"];
        sprite10.position = ccp( 420, 350);
        
        CCSprite * sprite11 = [CCSprite spriteWithSpriteFrameName:@"scene9_kaibutu2.png"];
        sprite11.position = ccp( 420, 350-73+1);
        
        CCSprite * sprite12 = [CCSprite spriteWithSpriteFrameName:@"scene9_kaibutu3.png"];
        sprite12.position = ccp( 420, 350-70-sprite11.contentSize.height+2);
        
        CCSprite * sprite13 = [CCSprite spriteWithSpriteFrameName:@"scene9_kaibutu4.png"];
        sprite13.position = ccp( 420, 350-70-sprite11.contentSize.height-sprite12.contentSize.height+3);
        
        [self addChild:sprite10];
        [self addChild:sprite11];
        [self addChild:sprite12];
        [self addChild:sprite13];
        
        particles[0] = [CCParticleSystemQuad particleWithFile:@"scene9_p.plist"];
        [self addChild:particles[0]];
        
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"frame.png"];
        sprite.position = ccp( 510, 384 );
        
        CCSprite * sprite2 = [CCSprite spriteWithSpriteFrameName:@"kaminari.png"];
        sprite2.position = ccp( 300, 400 );
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"kaminari2.png"];
        sprite3.position = ccp( 200, 404 );
        [self addChild:sprite3];
        
        CCSprite * sprite4 = [CCSprite spriteWithSpriteFrameName:@"kaminari3.png"];
        sprite4.position = ccp( 510, 384 );
        [self addChild:sprite4];
        
        CCSprite * sprite5 = [CCSprite spriteWithSpriteFrameName:@"kaminari4.png"];
        sprite5.position = ccp( 900, 384 );
        [self addChild:sprite5];
        
        CCSprite * sprite6 = [CCSprite spriteWithSpriteFrameName:@"scene9_kumo.png"];
        sprite6.position = ccp( 512, 650 );
        [self addChild:sprite6];
        
        CCSprite * sprite7 = [CCSprite spriteWithSpriteFrameName:@"scene9_hito.png"];
        sprite7.position = ccp( 650, -320);
        
        CCSprite * sprite8 = [CCSprite spriteWithSpriteFrameName:@"scene9_ame1.png"];
        sprite8.position = ccp( 512, 500);
        
        CCSprite * sprite9 = [CCSprite spriteWithSpriteFrameName:@"scene9_ame2.png"];
        sprite9.position = ccp( 512, 500);
        
        [self addChild:sprite7];
        [self addChild:sprite2];
        
        
        id brink1 = [CCBlink actionWithDuration:0.5 blinks:4];
        id brink2 = [CCBlink actionWithDuration:0.5 blinks:4];
        id brink3 = [CCBlink actionWithDuration:0.5 blinks:4];
        id brink4 = [CCBlink actionWithDuration:0.5 blinks:4];
        
        
        id Sec1 = [CCSequence actions:brink1,[CCDelayTime actionWithDuration:4.0f], nil];
        id rep1 = [CCRepeatForever actionWithAction: [[Sec1 copy] autorelease]];
        
        id Sec2 = [CCSequence actions:brink2,[CCDelayTime actionWithDuration:3.5f], nil];
        id rep2 = [CCRepeatForever actionWithAction: [[Sec2 copy] autorelease]];
        
        id Sec3 = [CCSequence actions:brink3,[CCDelayTime actionWithDuration:5.0f], nil];
        id rep3 = [CCRepeatForever actionWithAction: [[Sec3 copy] autorelease]];
        
        id Sec4 = [CCSequence actions:brink4,[CCDelayTime actionWithDuration:4.5f], nil];
        id rep4 = [CCRepeatForever actionWithAction: [[Sec4 copy] autorelease]];
        
        id Move = [CCMoveTo actionWithDuration:1.5 position:ccp(650,304)];
        id ease = [CCEaseSineOut actionWithAction:Move];
        id action = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],ease, nil];
        
        id Move2 = [CCMoveTo actionWithDuration:0.5 position:ccp(630,304)];
        id re = [CCMoveTo actionWithDuration:0.0 position:ccp(512,500)];
        id FadeOut = [CCFadeOut actionWithDuration:0.5];
        id action2 =[CCSpawn actions:Move2,FadeOut,nil];
        id action3 = [CCSequence actions:action2,re,nil];
        
        id Move3 = [CCMoveTo actionWithDuration:0.5 position:ccp(630,304)];
        id re2 = [CCMoveTo actionWithDuration:0.0 position:ccp(512,500)];
        id FadeOut2 = [CCFadeOut actionWithDuration:0.5];
        id action4 =[CCSpawn actions:Move3,FadeOut2,nil];
        id action5 = [CCSequence actions:[CCDelayTime actionWithDuration:0.4],action4,re2, nil];
        
        id rep5 = [CCRepeatForever actionWithAction: [[action3 copy] autorelease]];
        id rep6 = [CCRepeatForever actionWithAction: [[action5 copy] autorelease]];
        
        //魔物の動き
        id MamonoMove = [CCMoveTo actionWithDuration:1.0 position:ccp(445, 350)];
        id MamonoMove2 = [CCMoveTo actionWithDuration:1.0 position:ccp(395, 350)];
        id MamonoEase = [CCEaseSineInOut actionWithAction:MamonoMove];
        id MamonoEase2 = [CCEaseSineInOut actionWithAction:MamonoMove2];
        id MamonoAction =[CCSequence actions:MamonoEase,MamonoEase2, nil];
        id MamonoRep = [CCRepeatForever actionWithAction:[[MamonoAction copy] autorelease]];
        
        id MamonoMove3 = [CCMoveTo actionWithDuration:1.0 position:ccp(440, 350-70+1)];
        id MamonoMove4 = [CCMoveTo actionWithDuration:1.0 position:ccp(400, 350-70+1)];
        id MamonoEase3 = [CCEaseSineInOut actionWithAction:MamonoMove3];
        id MamonoEase4 = [CCEaseSineInOut actionWithAction:MamonoMove4];
        id MamonoAction2 =[CCSequence actions:MamonoEase3,MamonoEase4, nil];
        id MamonoRep2 = [CCRepeatForever actionWithAction:[[MamonoAction2 copy] autorelease]];
        
        id MamonoMove5 = [CCMoveTo actionWithDuration:1.0 position:ccp(435, 350-60-sprite11.contentSize.height+2)];
        id MamonoMove6 = [CCMoveTo actionWithDuration:1.0 position:ccp(405, 350-60-sprite11.contentSize.height+2)];
        id MamonoEase5 = [CCEaseSineInOut actionWithAction:MamonoMove5];
        id MamonoEase6 = [CCEaseSineInOut actionWithAction:MamonoMove6];
        id MamonoAction3 =[CCSequence actions:MamonoEase5,MamonoEase6, nil];
        id MamonoRep3 = [CCRepeatForever actionWithAction:[[MamonoAction3 copy] autorelease]];
        
        id MamonoMove7 = [CCMoveTo actionWithDuration:1.0 position:ccp(430, 350-55-sprite11.contentSize.height-sprite12.contentSize.height+3)];
        id MamonoMove8 = [CCMoveTo actionWithDuration:1.0 position:ccp(410, 350-55-sprite11.contentSize.height-sprite12.contentSize.height+3)];
        id MamonoEase7 = [CCEaseSineInOut actionWithAction:MamonoMove7];
        id MamonoEase8 = [CCEaseSineInOut actionWithAction:MamonoMove8];
        id MamonoAction4 =[CCSequence actions:MamonoEase7,MamonoEase8, nil];
        id MamonoRep4 = [CCRepeatForever actionWithAction:[[MamonoAction4 copy] autorelease]];
        
        [sprite10 runAction:MamonoRep];
        [sprite11 runAction:MamonoRep2];
        [sprite12 runAction:MamonoRep3];
        [sprite13 runAction:MamonoRep4];
        
        [sprite2 runAction:rep1];
        [sprite3 runAction:rep2];
        [sprite4 runAction:rep3];
        [sprite5 runAction:rep4];
        
        [sprite7 runAction:action];
        
        [self addChild:sprite9];
        [self addChild:sprite8];
        
        [sprite9 runAction:rep5];
        [sprite8 runAction:rep6];
        
        [self addChild:sprite];
        
    }else if(page == 10){
        
        if(!next){
            [SoundPlayer bgmStop:optionnum2[1]:2];
            [SoundPlayer bgmPlay:optionnum2[1]:4];
        }
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene10_fuufu.plist" textureFilename:@"scene10_fuufu.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene10_fuufu.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"scene9_ame1.png"];
        sprite.position = ccp( 512, 500);
        CCSprite * sprite2 = [CCSprite spriteWithSpriteFrameName:@"scene9_ame2.png"];
        sprite2.position = ccp( 512, 500);
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"scene10_fuufu.png"];
        sprite3.position = ccp( 780, -200);
        CCSprite * sprite4 = [CCSprite spriteWithSpriteFrameName:@"frame.png"];
        sprite4.position = ccp( 510, 384 );
        
        [self addChild:sprite3];
        [self addChild:sprite];
        [self addChild:sprite2];
        [self addChild:sprite4];
        
        id Move = [CCMoveTo actionWithDuration:0.5 position:ccp(630,304)];
        id re = [CCMoveTo actionWithDuration:0.0 position:ccp(512,500)];
        id FadeOut = [CCFadeOut actionWithDuration:0.5];
        id action2 =[CCSpawn actions:Move,FadeOut,nil];
        id action3 = [CCSequence actions:action2,re,nil];
        
        id Move2 = [CCMoveTo actionWithDuration:0.5 position:ccp(630,304)];
        id re2 = [CCMoveTo actionWithDuration:0.0 position:ccp(512,500)];
        id FadeOut2 = [CCFadeOut actionWithDuration:0.5];
        id action4 =[CCSpawn actions:Move2,FadeOut2,nil];
        id action5 = [CCSequence actions:[CCDelayTime actionWithDuration:0.4],action4,re2, nil];
        
        id rep5 = [CCRepeatForever actionWithAction: [[action3 copy] autorelease]];
        id rep6 = [CCRepeatForever actionWithAction: [[action5 copy] autorelease]];
        
        id Move3 = [CCMoveTo actionWithDuration:1.3 position:ccp(670,300)];
        id ease2 = [CCEaseSineOut actionWithAction:Move3];
        
        [sprite runAction:rep5];
        [sprite2 runAction:rep6];
        
        [sprite3 runAction:ease2];
        
    }else if(page == 11){
        
        //BGM読み込み
        [SoundPlayer bgmStop:optionnum2[1]:4];
        [SoundPlayer bgmPlay:optionnum2[1]:2];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene11_namida.plist" textureFilename:@"scene11_namida.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene11_namida.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"scene11_namida.png"];
        sprite.position =ccp(653,445);
        [self addChild:sprite];
        
        CCSprite *sprite2 = [CCSprite spriteWithSpriteFrameName:@"scene11_namida.png"];
        sprite2.position =ccp(683,450);
        [self addChild:sprite2];
        
        CCSprite *sprite3 = [CCSprite spriteWithSpriteFrameName:@"scene11_namida.png"];
        sprite3.position =ccp(381,458);
        [self addChild:sprite3];
        
        CCSprite *sprite4 = [CCSprite spriteWithSpriteFrameName:@"scene11_namida.png"];
        sprite4.position =ccp(403,459);
        [self addChild:sprite4];
        
        id Move = [CCMoveBy actionWithDuration: 1.5f position:ccp(0,-100.0f)]; 
        id Move2 = [CCMoveBy actionWithDuration: 0.0f position:ccp(0,100.0f)];
        id Fade = [CCFadeOut actionWithDuration: 1.5f];
        id Fade2 = [CCFadeOut actionWithDuration: 0.0f];
        id SineOut = [CCEaseExponentialIn actionWithAction:Move];
        id action = [CCSequence actions:[CCSpawn actions:SineOut,Fade,nil],Move2,Fade2,[CCDelayTime actionWithDuration:1.0f],nil];
        id rep = [CCRepeatForever actionWithAction: [[action copy] autorelease]];
        id rep3 = [CCRepeatForever actionWithAction: [[action copy] autorelease]];
        
        id Move3 = [CCMoveBy actionWithDuration: 1.4f position:ccp(0,-100)]; 
        id Move4 = [CCMoveBy actionWithDuration: 0.0f position:ccp(0,100)];
        id SineOut2 = [CCEaseExponentialIn actionWithAction:Move3];
        id action2 = [CCSequence actions:[CCSpawn actions:SineOut2,Fade,nil],Move4,Fade2,[CCDelayTime actionWithDuration:1.5f],nil];
        id rep2 = [CCRepeatForever actionWithAction: [[action2 copy] autorelease]];
        id rep4 = [CCRepeatForever actionWithAction: [[action2 copy] autorelease]];
        
        
        [sprite2 runAction:rep2];
        [sprite runAction:rep];
        [sprite3 runAction:rep3];
        [sprite4 runAction:rep4];
    }else if(page == 12){
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene12_kyoudai.plist" textureFilename:@"scene12_kyoudai.png"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene12_kyoudai.png"];
        [self addChild:spriteSheet];
        
        CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"kyoudai.png"];
        sprite.position =ccp(522,325);
        sprite.opacity = 0.0f;
        
        CCSprite *sprite2 = [CCSprite spriteWithSpriteFrameName:@"kyoudaihikari.png"];
        sprite2.position =ccp(512,350);
        sprite2.opacity = 0.0f;
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"frame.png"];
        sprite3.position = ccp( 510, 384 );
        
        id Fade = [CCFadeTo actionWithDuration:2.5f opacity:200];
        id FadeIn = [CCFadeIn actionWithDuration:3.5f];
        id FadeOut =[CCFadeOut actionWithDuration:3.5f];
        id Func =[CCCallFunc actionWithTarget:self selector:@selector(scene12)];
        id Action = [CCSequence actions:[CCDelayTime actionWithDuration:3.5f],FadeIn,FadeOut, nil];
        id Rep = [CCRepeatForever actionWithAction:[[Action copy] autorelease]];
        id Action2 = [CCSequence actions:[CCDelayTime actionWithDuration:3.5f],Func, nil];
        id Action3 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],Fade, nil];
        
        [self addChild:sprite2];
        [self addChild:sprite];
        [self addChild:sprite3];
        
        [sprite2 runAction:Rep];
        [sprite runAction:Action3];
        [self runAction:Action2];
        
    }else if(page == 13){
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene13_obj.plist" textureFilename:@"scene13_obj.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene13_obj.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"scene13_kumo.png"];
        sprite.position =ccp(512,300);
        [self addChild:sprite];
        
        CCSprite *sprite2 = [CCSprite spriteWithSpriteFrameName:@"scene13_obj.png"];
        sprite2.position =ccp(512,325);
        [self addChild:sprite2];
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"frame.png"];
        sprite3.position = ccp( 510, 384 );
        [self addChild:sprite3];
        
        id Move = [CCMoveBy actionWithDuration:4.0 position:ccp(0,280)];
        id Ease = [CCEaseSineOut actionWithAction:Move];
        
        [sprite runAction:Ease];
        
        
    }else if(page == 14){
        
        if(!next){
            [SoundPlayer bgmStop:optionnum2[1]:3];
            [SoundPlayer bgmPlay:optionnum2[1]:2];
        }
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"page14_hiwaku.plist" textureFilename:@"page14_hiwaku.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"page14_hiwaku.pvr.gz"];
        [self addChild:spriteSheet];
        
        
        CCSpriteFrameCache *frameCache2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache2 addSpriteFramesWithFile:@"page14_hiwaku2.plist" textureFilename:@"page14_hiwaku2.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet2 = [CCSpriteBatchNode batchNodeWithFile:@"page14_hiwaku2.pvr.gz"];
        [self addChild:spriteSheet2];
        
        CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"scene14_hiwaku.png"];
        sprite.position = ccp( 512, 200 );
        [self addChild:sprite z:7];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"scene14_hiwaku2.png"];
        Movie_Sprite[0].position =ccp(140,202);
        [self addChild:Movie_Sprite[0] z:8];
        
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"scene14_hiwaku2.png"];
        Movie_Sprite[1].position =ccp(873,202);
        [self addChild:Movie_Sprite[1] z:9];
        
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"scene14_hiwaku2.png"];
        Movie_Sprite[2].position =ccp(282,240);
        Movie_Sprite[2].scale = 0.55;
        [self addChild:Movie_Sprite[2] z:10];
        
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"scene14_hiwaku2.png"];
        Movie_Sprite[3].position =ccp(750,240);
        Movie_Sprite[3].scale = 0.55;
        [self addChild:Movie_Sprite[3] z:11];
        
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"scene14_hiwaku2.png"];
        Movie_Sprite[4].position =ccp(375,260);
        Movie_Sprite[4].scaleX = 0.32;
        Movie_Sprite[4].scaleY = 0.42;
        [self addChild:Movie_Sprite[4] z:12];
        
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"scene14_hiwaku2.png"];
        Movie_Sprite[5].position =ccp(631,260);
        Movie_Sprite[5].scaleX = 0.32;
        Movie_Sprite[5].scaleY = 0.42;
        [self addChild:Movie_Sprite[5] z:13];
        
    }else if(page == 15){
        [self preloadParticleEffect:@"scene15_particle1.plist"];
        [self preloadParticleEffect:@"scene15_particle2.plist"];
        [self preloadParticleEffect:@"scene15_particle3.plist"];
        [self preloadParticleEffect:@"scene15_particle4.plist"];
        [self preloadParticleEffect:@"scene15_particle5.plist"];
        [self preloadParticleEffect:@"scene15_particle6.plist"];
        
        //BGM
        if(!next){
            [SoundPlayer bgmStop:optionnum2[1]:4];
            [SoundPlayer bgmPlay:optionnum2[1]:3];
        }else{
            [SoundPlayer bgmStop:optionnum2[1]:6];
            [SoundPlayer bgmStop:optionnum2[1]:2];
            [SoundPlayer bgmPlay:optionnum2[1]:3];
        }
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene15_pict.plist" textureFilename:@"scene15_pict.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene15_pict.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCParticleSystem *particle;
        particle = [CCParticleSystemQuad particleWithFile:@"scene15_particle1.plist"];
        [self addChild:particle];
        
        CCParticleSystem *particle2;
        particle2 = [CCParticleSystemQuad particleWithFile:@"scene15_particle2.plist"];
        [self addChild:particle2];
        
        CCParticleSystem *particle3;
        particle3 = [CCParticleSystemQuad particleWithFile:@"scene15_particle3.plist"];
        [self addChild:particle3];
        
        CCParticleSystem *particle4;
        particle4 = [CCParticleSystemQuad particleWithFile:@"scene15_particle4.plist"];
        [self addChild:particle4];
        
        CCParticleSystem *particle5;
        particle5 = [CCParticleSystemQuad particleWithFile:@"scene15_particle5.plist"];
        [self addChild:particle5];
        
        CCSprite *sprite2 = [CCSprite spriteWithSpriteFrameName:@"hiwaku2.png"];
        sprite2.position =ccp(512,400);
        [self addChild:sprite2];
        
        CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"murabito.png"];
        sprite.position =ccp(552,330);
        [self addChild:sprite];
        
        CCParticleSystem *particle6;
        particle6 = [CCParticleSystemQuad particleWithFile:@"scene15_particle6.plist"];
        [self addChild:particle6];
        
        CCSprite *sprite3 = [CCSprite spriteWithSpriteFrameName:@"hiwaku1.png"];
        sprite3.position =ccp(812,200);
        [self addChild:sprite3];
        
    }else if(page == 16){
        
        [self preloadParticleEffect:@"scene16_particle.plist"];
        [self preloadParticleEffect:@"scene16_particle2.plist"];
        [self preloadParticleEffect:@"scene16_particle3.plist"];
        [self preloadParticleEffect:@"scene16_particle4.plist"];
        
        [SoundPlayer bgmStop:optionnum2[1]:2];
        [SoundPlayer bgmStop:optionnum2[1]:3];
        [SoundPlayer bgmPlay:optionnum2[1]:4];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene8_kaijyu.plist" textureFilename:@"scene8_kaijyu.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene8_kaijyu.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSpriteFrameCache *frameCache2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache2 addSpriteFramesWithFile:@"scene16_nami.plist" textureFilename:@"scene16_nami.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet2 = [CCSpriteBatchNode batchNodeWithFile:@"scene16_nami.pvr.gz"];
        [self addChild:spriteSheet2];
        
        
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu4.png"];
        Movie_Sprite[3].position =ccp(512,235);
        [self addChild:Movie_Sprite[3]];
        
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu3.png"];
        Movie_Sprite[2].position =ccp(512,350);
        [self addChild:Movie_Sprite[2]];
        
        Movie_Sprite[1]  = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu2.png"];
        Movie_Sprite[1].position =ccp(512,428);
        [self addChild:Movie_Sprite[1]];
        
        Movie_Sprite[0]  = [CCSprite spriteWithSpriteFrameName:@"ishiki08_kaijyu1.png"];
        Movie_Sprite[0] .position =ccp(512,580);
        [self addChild: Movie_Sprite[0]];
        
        particles[7] = [CCParticleSystemQuad particleWithFile:@"scene16_particle.plist"];
        [self addChild:particles[7]];
        
        
        //Button用のSprite
        Movie_Sprite[4]  = [CCSprite spriteWithFile:@"ishiki16_kari.png"];
        Movie_Sprite[4] .position =ccp(170,600);
        [self addChild: Movie_Sprite[4]];
        
        Movie_Sprite[5]  = [CCSprite spriteWithFile:@"ishiki16_kari.png"];
        Movie_Sprite[5] .position =ccp(210,200);
        [self addChild: Movie_Sprite[5]];
        
        Movie_Sprite[6]  = [CCSprite spriteWithFile:@"ishiki16_kari.png"];
        Movie_Sprite[6] .position =ccp(830,250);
        [self addChild: Movie_Sprite[6]];
        
        
        
        id MamonoMove = [CCMoveTo actionWithDuration:1.0 position:ccp(602, 580)];
        id MamonoMove2 = [CCMoveTo actionWithDuration:1.0 position:ccp(422, 580)];
        id MamonoEase = [CCEaseSineInOut actionWithAction:MamonoMove];
        id MamonoEase2 = [CCEaseSineInOut actionWithAction:MamonoMove2];
        id MamonoAction =[CCSequence actions:MamonoEase,MamonoEase2, nil];
        MamonoRep16[0] = [CCRepeatForever actionWithAction:[[MamonoAction copy] autorelease]];
        
        id MamonoMove3 = [CCMoveTo actionWithDuration:1.0 position:ccp(582, 428)];
        id MamonoMove4 = [CCMoveTo actionWithDuration:1.0 position:ccp(442, 428)];
        id MamonoEase3 = [CCEaseSineInOut actionWithAction:MamonoMove3];
        id MamonoEase4 = [CCEaseSineInOut actionWithAction:MamonoMove4];
        id MamonoAction2 =[CCSequence actions:MamonoEase3,MamonoEase4, nil];
        MamonoRep16[1] = [CCRepeatForever actionWithAction:[[MamonoAction2 copy] autorelease]];
        
        id MamonoMove5 = [CCMoveTo actionWithDuration:1.0 position:ccp(552, 350)];
        id MamonoMove6 = [CCMoveTo actionWithDuration:1.0 position:ccp(472, 350)];
        id MamonoEase5 = [CCEaseSineInOut actionWithAction:MamonoMove5];
        id MamonoEase6 = [CCEaseSineInOut actionWithAction:MamonoMove6];
        id MamonoAction3 =[CCSequence actions:MamonoEase5,MamonoEase6, nil];
        MamonoRep16[2] = [CCRepeatForever actionWithAction:[[MamonoAction3 copy] autorelease]];
        
        id MamonoMove7 = [CCMoveTo actionWithDuration:1.0 position:ccp(522, 235)];
        id MamonoMove8 = [CCMoveTo actionWithDuration:1.0 position:ccp(512, 235)];
        id MamonoEase7 = [CCEaseSineInOut actionWithAction:MamonoMove7];
        id MamonoEase8 = [CCEaseSineInOut actionWithAction:MamonoMove8];
        id MamonoAction4 =[CCSequence actions:MamonoEase7,MamonoEase8, nil];
        MamonoRep16[3] = [CCRepeatForever actionWithAction:[[MamonoAction4 copy] autorelease]];
        
        
        
        [Movie_Sprite[0] runAction:MamonoRep16[0]];
        [Movie_Sprite[1] runAction:MamonoRep16[1]];
        [Movie_Sprite[2] runAction:MamonoRep16[2]];
        [Movie_Sprite[3] runAction:MamonoRep16[3]];
        
        CCSprite *sprite  = [CCSprite spriteWithSpriteFrameName:@"scene16_nami.png"];
        sprite .position =ccp(512,195);
        [self addChild: sprite];
        
        CCSprite * sprite3 = [CCSprite spriteWithSpriteFrameName:@"frame.png"];
        sprite3.position = ccp( 510, 384 );
        [self addChild:sprite3];
        
    }else if(page == 17){
        [self preloadParticleEffect:@"scene17_particle1.plist"];
        [self preloadParticleEffect:@"scene17_particle2.plist"];
        [self preloadParticleEffect:@"scene17_particle3.plist"];
        [self preloadParticleEffect:@"scene17_particle4.plist"];
        
        
        //BGM
        if(!next){
            [SoundPlayer bgmStop:optionnum2[1]:1];
            [SoundPlayer bgmPlay:optionnum2[1]:3];
        }else{
            [SoundPlayer bgmStop:optionnum2[1]:4];
            [SoundPlayer bgmPlay:optionnum2[1]:3];
        }
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene17_pict.plist" textureFilename:@"scene17_pict.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene17_pict.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCParticleSystem *particle;
        particle = [CCParticleSystemQuad particleWithFile:@"scene17_particle1.plist"];
        [self addChild:particle];
        
        CCParticleSystem *particle2;
        particle2 = [CCParticleSystemQuad particleWithFile:@"scene17_particle2.plist"];
        [self addChild:particle2];
        
        CCParticleSystem *particle3;
        particle3 = [CCParticleSystemQuad particleWithFile:@"scene17_particle3.plist"];
        [self addChild:particle3];
        
        CCParticleSystem *particle4;
        particle4 = [CCParticleSystemQuad particleWithFile:@"scene17_particle4.plist"];
        [self addChild:particle4];
        
        CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"hiwaku4.png"];
        sprite.position =ccp(522,180);
        [self addChild:sprite];
        
        CCSprite *sprite2 = [CCSprite spriteWithSpriteFrameName:@"murabito2.png"];
        sprite2.position =ccp(522,240);
        [self addChild:sprite2];
        
    }else if(page == 18){
        
        [SoundPlayer bgmStop:optionnum2[1]:4];
        [SoundPlayer bgmPlay:optionnum2[1]:1];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene18_object.plist" textureFilename:@"scene18_object.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene18_object.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSprite *sprite3 = [CCSprite spriteWithSpriteFrameName:@"tyoutin3.png"];
        sprite3.position =ccp(420,410);
        sprite3.opacity = 0.0f;
        [self addChild:sprite3];
        
        CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"tyoutin1.png"];
        sprite.position =ccp(220,370);
        sprite.opacity = 0.0f;
        [self addChild:sprite];
        
        CCSprite *sprite2 = [CCSprite spriteWithSpriteFrameName:@"tyoutin2.png"];
        sprite2.position =ccp(645,370);
        sprite2.opacity = 0.0f;
        [self addChild:sprite2];
        
        id FadeIn = [CCFadeIn actionWithDuration:1.5f];
        id FadeIn2 = [CCFadeIn actionWithDuration:1.5f];
        id FadeIn3 = [CCFadeIn actionWithDuration:1.5f];
        
        id Action = [CCSequence actions:[CCDelayTime actionWithDuration:1.5f],FadeIn, nil];
        id Action2 = [CCSequence actions:[CCDelayTime actionWithDuration:2.5f],FadeIn2, nil];
        id Action3 = [CCSequence actions:[CCDelayTime actionWithDuration:3.5f],FadeIn3, nil];
        [sprite2 runAction:Action];
        [sprite runAction:Action2];
        [sprite3 runAction:Action3];
        
    }else if(page == 19){
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene19_object.plist" textureFilename:@"scene19_object.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene19_object.pvr.gz"];
        [self addChild:spriteSheet];
        
        P19_Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"isshiki19_left_foot.png"];
        [self addChild:P19_Movie_Sprite[2]];
        
        P19_Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"isshiki19_left_body.png"];
        [self addChild:P19_Movie_Sprite[1]];
        
        P19_Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"isshiki19_left_head.png"];
        [self addChild:P19_Movie_Sprite[0]];
        
        P19_Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"isshiki19_right_foot.png"];
        [self addChild:P19_Movie_Sprite[5]];
        
        P19_Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"isshiki19_right_body.png"];
        [self addChild:P19_Movie_Sprite[4]];
        
        P19_Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"isshiki19_right_head.png"];
        [self addChild:P19_Movie_Sprite[3]];
        
        [self schedule: @selector(tick:)];
        [self addNewSpriteWithJoint2];
        
    }else if(page == 22){
        if(!next){
            [SoundPlayer bgmStop:optionnum2[1]:7];
            [SoundPlayer bgmPlay:optionnum2[1]:1];
        }
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"scene22_tono.plist" textureFilename:@"scene22_tono.pvr.gz"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"scene22_tono.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0]= [CCSprite spriteWithSpriteFrameName:@"tono.png"];
        Movie_Sprite[0].position =ccp(512,300);
        [self addChild: Movie_Sprite[0]];
        
        [self addNewSpriteWithCoords];
    }else if(page == 23){
        
        [SoundPlayer bgmStop:optionnum2[1]:1];
        [SoundPlayer bgmPlay:optionnum2[1]:7];
    }
}

#pragma mark -
#pragma mark シーン2:胸を叩くエフェクト
-(void)sece2:(int)num{
    touche =false;
    if(num == 1){
        Movie_Sprite[0].tag = 1;
        id Scale = [CCScaleBy actionWithDuration:0.2 scaleX:0.6f scaleY:1.0f];
        id ease =[CCEaseSineInOut actionWithAction:Scale];
        id func = [CCCallFuncN actionWithTarget:self selector:@selector(Particle:)];
        id Scale2 = [ease reverse];
        id Action =[CCSequence actions:ease,Scale2,func,nil];
        [Movie_Sprite[0] runAction:Action];
    }else{
        Movie_Sprite[1].tag = 2;
        id Scale3 = [CCScaleBy actionWithDuration:0.2 scaleX:0.6f scaleY:1.0f];
        id ease2 =[CCEaseSineInOut actionWithAction:Scale3];
        id func2 = [CCCallFuncN actionWithTarget:self selector:@selector(Particle:)];
        id Scale4 = [ease2 reverse];
        id Action2 =[CCSequence actions:ease2,Scale4,func2,nil];
        [Movie_Sprite[1] runAction:Action2];
    }
}


#pragma mark パーティクル
-(void)Particle:(id)sender{
    [SoundPlayer PlaySE:optionnum2[1]:1];
    
    if([sender tag] == 1){
        CCParticleSystem *particle;
        particle = [CCParticleSystemQuad particleWithFile:@"scene2_p.plist"];
        [self addChild:particle z:1];
    }else{
        CCParticleSystem *particle2;
        particle2 = [CCParticleSystemQuad particleWithFile:@"scene2_p2.plist"];
        [self addChild:particle2 z:0];
    }
    touche =true;
    
}

-(void)scene6{
    Movie_Sprite[0].position = ccp( 1124, 620 );
    Movie_Sprite[1].position = ccp( 1624, 610 );
    Movie_Sprite[2].position = ccp( 1324, 480 );
    Movie_Sprite[3].position = ccp( 1124, 550 );
    
    id Move = [CCMoveBy actionWithDuration:20.0 position:ccp(-1324,0)];
    id Move2 = [CCMoveBy actionWithDuration:25.0 position:ccp(-1824,0)];
    id Move3 = [CCMoveBy actionWithDuration:22.0 position:ccp(-1524,0)];
    id Move4 = [CCMoveBy actionWithDuration:28.0 position:ccp(-1324,0)];
    id func = [CCCallFunc actionWithTarget:self selector:@selector(scene6)];
    id action =[CCSequence actions:Move4,func,nil];
    
    [Movie_Sprite[0] runAction:Move];
    [Movie_Sprite[1] runAction:Move2];
    [Movie_Sprite[2] runAction:Move3];
    [Movie_Sprite[3] runAction:action];
}

-(void)scene12{
    [self preloadParticleEffect:@"scene12_particle.plist"];
    CCParticleSystem *particle;
    particle = [CCParticleSystemQuad particleWithFile:@"scene12_particle.plist"];
    [self addChild:particle z:-1];
}

-(void)scene14:(NSInteger)num{
    [self preloadParticleEffect:@"scene14_particle.plist"];
    [self preloadParticleEffect:@"scene14_particle2.plist"];
    [self preloadParticleEffect:@"scene14_particle3.plist"];
    [self preloadParticleEffect:@"scene14_particle4.plist"];
    [self preloadParticleEffect:@"scene14_particle5.plist"];
    [self preloadParticleEffect:@"scene14_particle6.plist"];
    
    if(!SoundContorl_takibi){
        [SoundPlayer bgmPlay:optionnum2[1]:6];
        SoundContorl_takibi = true;
    }
    
    if(num==1 && !firest1){
        if(optionnum2[1] == 0){
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"Fire1.mp3"];
        }
        CCParticleSystem *particle;
        particle = [CCParticleSystemQuad particleWithFile:@"scene14_particle.plist"];
        [self addChild:particle z:1];
        firest1 = true;
        
    }else if(num==2  && !firest2){
        if(optionnum2[1] == 0){
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"Fire1.mp3"];
        }
        CCParticleSystem *particle2;
        particle2 = [CCParticleSystemQuad particleWithFile:@"scene14_particle4.plist"];
        [self addChild:particle2 z:2];
        firest2 = true;
        
    }else if(num==3  && !firest3){
        if(optionnum2[1] == 0){
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"Fire1.mp3"];
        }
        CCParticleSystem *particle3;
        particle3 = [CCParticleSystemQuad particleWithFile:@"scene14_particle2.plist"];
        [self addChild:particle3 z:3];
        firest3 = true;
        
    }else if(num==4  && !firest4){
        if(optionnum2[1] == 0){
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"Fire1.mp3"];
        }
        CCParticleSystem *particle4;
        particle4 = [CCParticleSystemQuad particleWithFile:@"scene14_particle5.plist"];
        [self addChild:particle4 z:4];
        firest4 = true;
        
    }else if(num==5  && !firest5){
        if(optionnum2[1] == 0){
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"Fire1.mp3"];
        }
        CCParticleSystem *particle5;
        particle5 = [CCParticleSystemQuad particleWithFile:@"scene14_particle3.plist"];
        [self addChild:particle5 z:5];
        firest5 = true;
        
    }else if(num==6  && !firest6){
        if(optionnum2[1] == 0){
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"Fire1.mp3"];
        }
        CCParticleSystem *particle6;
        particle6 = [CCParticleSystemQuad particleWithFile:@"scene14_particle6.plist"];
        [self addChild:particle6 z:6];
        firest6 = true;
        
    }
}

#pragma mark -
#pragma mark シーン16:炎が飛んでいき魔王が点滅
-(void)scene16:(NSInteger)num:(CGPoint)p{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    particles[maouPower] = [CCParticleSystemQuad particleWithFile:@"scene16_particle2.plist"];
    particles[maouPower].position = ccp(p.x,p.y); 
    [self addChild:particles[maouPower]];
     

    id Move = [CCMoveTo actionWithDuration:1.5f position:ccp(winSize.width/2,winSize.height/2)];
     id fireEase = [CCEaseSineIn actionWithAction:Move];
     [particles[maouPower] runAction:fireEase];
     
     id brink1 = [CCBlink actionWithDuration:1.0f blinks:5];
     id brink2 = [CCBlink actionWithDuration:1.0f blinks:5];
     id brink3 = [CCBlink actionWithDuration:1.0f blinks:5];
     id brink4 = [CCBlink actionWithDuration:1.0f blinks:5];
     id Action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.5f],brink1, nil];
     id Action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.5f],brink2, nil];
     id Action3 = [CCSequence actions:[CCDelayTime actionWithDuration:1.5f],brink3, nil];
     id Action4 = [CCSequence actions:[CCDelayTime actionWithDuration:1.5f],brink4, nil];
     
     id Func1 = [CCCallFuncN actionWithTarget:self selector:@selector(scene16HinotamaSyoumetu:)];
     id Action5 = [CCSequence actions:[CCDelayTime actionWithDuration:1.5f],Func1, nil];
     
     [Movie_Sprite[0] runAction:Action1];
     [Movie_Sprite[1] runAction:Action2];
     [Movie_Sprite[2] runAction:Action3];
     [Movie_Sprite[3] runAction:Action4];
     [particles[maouPower] runAction:Action5];
    particles[maouPower].tag = maouPower;
     maouPower = maouPower-1;
    
     //魔王のパワー判断
     if(maouPower == 0){
         id Func = [CCCallFunc actionWithTarget:self selector:@selector(scene16Funsai)];
         id Action = [CCSequence actions:[CCDelayTime actionWithDuration:2.5f],Func, nil];
         
         [self runAction:Action];
     }
     }
     
#pragma mark -
#pragma mark シーン16火の玉消滅
     -(void)scene16HinotamaSyoumetu:(id)sender{
         CCLOG(@"%d",[sender tag]);
         if(optionnum2[1] == 0){
             se = [[SimpleAudioEngine sharedEngine] playEffect:@"bomb.mp3"];
         }
         [particles[[sender tag]] removeFromParentAndCleanup:YES];
     }
     
#pragma mark -
#pragma mark シーン16魔王粉砕
     -(void)scene16Funsai{
         [Movie_Sprite[0] stopAction:MamonoRep16[0]];
         [Movie_Sprite[1] stopAction:MamonoRep16[1]];
         [Movie_Sprite[2] stopAction:MamonoRep16[2]];
         [Movie_Sprite[3] stopAction:MamonoRep16[3]];
         if(optionnum2[1] == 0){
             se = [[SimpleAudioEngine sharedEngine] playEffect:@"monstar2.mp3"];
         }
         
         
         id MamonoMove1 = [CCMoveTo actionWithDuration:1.0f position:ccp(Movie_Sprite[0].position.x, 80)];
         id MamonoMove2 = [CCMoveTo actionWithDuration:1.0f position:ccp(Movie_Sprite[1].position.x, 80)];
         id MamonoMove3 = [CCMoveTo actionWithDuration:1.0f position:ccp(Movie_Sprite[2].position.x, 80)];
         id MamonoMove4 = [CCMoveTo actionWithDuration:1.0f position:ccp(Movie_Sprite[3].position.x, 80)];
         
         id Ease1 = [CCEaseSineIn actionWithAction:MamonoMove1];
         id Ease2 = [CCEaseSineIn actionWithAction:MamonoMove2];
         id Ease3 = [CCEaseSineIn actionWithAction:MamonoMove3];
         id Ease4 = [CCEaseSineIn actionWithAction:MamonoMove4];
         id Func1 = [CCCallFunc actionWithTarget:self selector:@selector(scene16TatumakiStop)];
         
         id Action1 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],Func1,Ease1,nil];
         id Action2 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],Ease2,nil];
         id Action3 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],Ease3,nil];
         id Action4 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],Ease4,nil];
         
         [Movie_Sprite[0] runAction:Action1];
         [Movie_Sprite[1] runAction:Action2];
         [Movie_Sprite[2] runAction:Action3];
         [Movie_Sprite[3] runAction:Action4];
         
     }
#pragma mark -
#pragma mark シーン16魔王竜巻消去
     -(void)scene16TatumakiStop{
         [particles[7] removeFromParentAndCleanup:YES];
     }
     
#pragma mark -
#pragma mark ナレーション音再生
-(void)Playnarration{
    if(optionnum2[0] == 0){
        [SoundPlayer PlayNarration:page];
    }
}

-(void)check{
    touche =true;
}

//Spriteの衝突判定用
-(CGRect)rectForSprite:(CCSprite *)sprite{
    
    CGRect rect;
    
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    if (page == 2) {
        float x = sprite.position.x;
        float y = sprite.position.y;
        rect = CGRectMake(x,y,w,h);
    }else{
        float x = sprite.position.x-w/2;
        float y = sprite.position.y-h/2;
        rect = CGRectMake(x,y,w,h);
        
    }
    
    
    return rect;
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    kaneTouch = [myTouch locationInView:[myTouch view]];
    
    if(page == 2 && touche) {
        location =[[CCDirector sharedDirector] convertToGL:location];
        CGRect imageRect = [self rectForSprite:Movie_Sprite[0]];
        CGRect imageRect2 = [self rectForSprite:Movie_Sprite[1]];
        if(CGRectContainsPoint(imageRect, location)){
            [self sece2:1];
        }else if(CGRectContainsPoint(imageRect2, location)){
            [self sece2:2];
        }
    }
    
    if(page==8){
        if (_mouseJoint != NULL) return;
        kaneTouch = [myTouch locationInView:[myTouch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        
        for (int i=0; i<4; i++) {
            if (_paddleFixture[i] -> TestPoint(locationWorld)) {
                b2MouseJointDef md;
                md.bodyA = groundBody;
                md.bodyB = m_bodies[i];
                md.target = locationWorld;
                md.collideConnected = false;
                md.maxForce = 1000.0f * m_bodies[i]->GetMass();
                _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                m_bodies[i]->SetAwake(true);
            }
            
        }
    }
    
    
    //シーン14のタッチポイントの算出
    if(page == 14) {
        location =[[CCDirector sharedDirector] convertToGL:location];
        CGRect imageRect = [self rectForSprite:Movie_Sprite[0]];
        CGRect imageRect2 = [self rectForSprite:Movie_Sprite[1]];
        CGRect imageRect3 = [self rectForSprite:Movie_Sprite[2]];
        CGRect imageRect4 = [self rectForSprite:Movie_Sprite[3]];
        CGRect imageRect5 = [self rectForSprite:Movie_Sprite[4]];
        CGRect imageRect6 = [self rectForSprite:Movie_Sprite[5]];
        
        if(CGRectContainsPoint(imageRect, location)){
            [self scene14:1];
        }else if(CGRectContainsPoint(imageRect2, location)){
            [self scene14:2];
        }else if(CGRectContainsPoint(imageRect3, location)){
            [self scene14:3];
        }else if(CGRectContainsPoint(imageRect4, location)){
            [self scene14:4];
        }else if(CGRectContainsPoint(imageRect5, location)){
            [self scene14:5];
        }else if(CGRectContainsPoint(imageRect6, location)){
            [self scene14:6];
        }
    }
    
    //シーン16のタッチポイントの算出
    if(page == 16 && maouPower > 0) {
        location =[[CCDirector sharedDirector] convertToGL:location];
        if(optionnum2[1] == 0){
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"honou.mp3"];
        }
        [self scene16:1:location];
    }
    
    //シーン19:喧嘩
    if(page==19){
        if (_mouseJoint != NULL) return;
        location = [[CCDirector sharedDirector] convertToGL:location];
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        
        for (int i=0; i<6; i++) {
            if (_paddleFixture[i] -> TestPoint(locationWorld)) {
                b2MouseJointDef md;
                md.bodyA = groundBody;
                md.bodyB = m_bodies[i];
                md.target = locationWorld;
                md.collideConnected = false;
                md.maxForce = 1000.0f * m_bodies[i]->GetMass();
                _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                m_bodies[i]->SetAwake(true);
            }
        }
    }
    
    //シーン22のタッチポイントの算出
    if(page == 22) {
        location = [[CCDirector sharedDirector] convertToGL:location];
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        kaneTouch = [myTouch locationInView:[myTouch view]];
        if (_paddleFixture[0] -> TestPoint(locationWorld)) {
            b2MouseJointDef md;
            md.bodyA = groundBody;
            md.bodyB = body;
            md.target = locationWorld;
            md.collideConnected = true;
            md.maxForce = 1000.0 * body->GetMass();
            _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
            body->SetAwake(true);
        }
        
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;
    
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    _mouseJoint->SetTarget(locationWorld);
    
}
-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}


- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    
    if (_mouseJoint) {
        int loc_x = location.x-kaneTouch.x;
        int loc_y = location.y-kaneTouch.y;
        
        
        BOOL Move =false;
        if(loc_x >= 50 || loc_y >= 50 || loc_x<-50 || loc_y<-50){
            Move = true;
        }
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
        if(optionnum2[1] == 0){
            if(page == 8 && Move){
                [SoundPlayer PlaySE:optionnum2[1]:0];
            }else if(page == 22 && Move){
                [SoundPlayer PlaySE:optionnum2[1]:2];
            }
        }
    }
}

//パーティクルのpreload
-(void)preloadParticleEffect:(NSString *)particleFile{
    [CCParticleExplosion particleWithFile:particleFile];
}



#pragma mark Accelerometer Input
- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{
    
    /*
     body->SetAwake(true);
     #define kPlayerSpeed 10
     #define kPlayerSpeedX 5
     float prevY,prevX;
     prevY = -acceleration.y * kPlayerSpeed;
     prevX = acceleration.x * kPlayerSpeedX;
     b2Vec2 gravity(prevY, prevX);
     world->SetGravity(gravity);
     */
    
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    // in case you have something to dealloc, do it in this method
    //CCLOG(@"ここで終わり%@:%@:ページ%d",NSStringFromSelector(_cmd),self,page);
    delete world;
    world = NULL;
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
    delete m_debugDraw;
	m_debugDraw = NULL;
    
    [super dealloc];
}
@end
